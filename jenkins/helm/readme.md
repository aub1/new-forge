# Deploiement de Jenkins


Le service Jenkins doit être accessible depuis l'extérieur à l'url : `https://jenkins.new-forge.squashtest.org:443`



## Manifeste 

Les éléments suivants sont provisionnés par le présent code : 


### Helm values pour Jenkins : 

* plugins : plugins de base + kubernetes-credentials-provider, config-file-provider

### Plomberie

* secret-reader-binding : va donner le clusterrole `secret-reader` au service Jenkins, mais uniquement dans son namespace. Ceci va permettre de gérer les managed-credentials via Kubernetes (voir ci-dessous).

### managed-credentials :

Il s'agit des comptes applicatifs utilisés par Jenkins pour se connecter aux autres outils. 

* squashtest-bitbucket-credentials : clé ssh du compte maître `squashtest` sur `https://bitbucket.org/squashtest/`
* jenkins-nexus-credentials : login/token de connexion à utiliser par Jenkins pour Nexus

**NOTE** : les vrais secrets ne sont bien sûr pas commité avec ce code ! Ne sont proposés ici que les templates, mais le contenu réel des secrets devra être renseigné à la main avant de les soumettre au plan de contrôle.


## Procédure de déploiement

Voir documentation : https://github.com/jenkinsci/helm-charts/blob/main/charts/jenkins/README.md
A exécuter depuis ce répertoire (jenkins/helm).

### Préambule

Avant exécution, vérifier que les ressources du `top-level` ont bien été installées et que 
les `managed-credentials` ont été valorisés.

Le montage de Jenkins est un peu particulier parce qu'on souhaite le faire en deux étapes :
* D'abord avec la feature de Jenkins Configuration as Code (JCasC) actif, afin que la connection entre Kubernetes et Jenkins 
soit préconfigurée 
* Puis désactiver JCasC et redémarrer Jenkins, afin que cette feature ne provoque pas d'effets inattendus au cours de l'exploitation.


### Etape 1 : initialiser 

Installer si nécessaire le repo Helm :

    helm repo add jenkins https://charts.jenkins.io
    helm repo update

Puis modifier `jenkins-values.yaml` de sorte à réactiver JCasC : 
    
```yaml
  initializeOnce: false
  JCasC:
    enabled: true
  sidecars:
    configAutoReload:
      enabled: true
``` 

Puis dérouler les commandes :

    kubectl create ns jenkins
    helm install jenkins jenkins/jenkins -f jenkins-values.yaml -n jenkins 
    kubectl -n jenkins apply -f secret-reader-binding.yaml


Une fois que Jenkins est démarré on doit se connecter dessus. Le mot de passe est le suivant : 

    kubectl -n jenkins get secret jenkins -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode; echo

On vérifie maintenant que Jenkins a bien configuré les accès Kubernetes :

* Aller à https://jenkins.new-forge.squashtest.org/configureClouds/
* Vérifier que le cluster `Kubernetes` est bien déclaré.  

### Etape 2 : configurer un compte admin local

Ici nous allons changer la politique de sécurité de Jenkins, qui pour l'instant délègue à Kubernetes. Il est nécessaire 
de procéder de cette façon car nous voulons à terme que Jenkins ne soit plus géré par Kubernetes, afin d'éviter les 
mauvaises surprises :

* Aller à `https://jenkins.new-forge.squashtest.org/configureSecurity/`
* Changer l'authentication pour passer en mode `Base de données des utilisateurs Jenkins` 

### Etape 3 : désactiver JCasC

Nous allons maintenant désactiver JCasc. C'est l'opération inverse de ci-dessus :

```yaml
  initializeOnce: true
  JCasC:
    enabled: false
  sidecars:
    configAutoReload:
      enabled: false
``` 

Puis mettre à jour la helm release sur le cluster : 

    helm upgrade jenkins jenkins/jenkins -f jenkins-values.yaml -n jenkins
    
### Etape 3 : terminer la configuration


#### Configuration du Cloud Kubernetes :

1. Aller à `https://jenkins.new-forge.squashtest.org/configureClouds/`
2. Dans les options globales du cluster : dans la section Pod Label, déclarer le label :
    - key : `velero.io/exclude-from-backup`
    - value : `true` 
3. Definition du pod `default` : changer la command et arg pour `jenkins-agent` et `` (vide)
4. Déclarer les pod templates, qui sont situés dans `jenkins/pod-templates`

