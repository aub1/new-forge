
# But du Nexus Public

Nexus Public est l'instance qui propose au public les artifacts gratuits et libres de téléchargement. Il vit en dehors 
de la DMZ et n'a pas besoin de backup. Il peut être pwned sans conséquences pour notre organisation.

Il propose trois dépôts en accès anonyme : 

* maven-public-releases : il propose les artifacts maven.
* public-releases : propose les déployables (cad distributions) de nos softwares en version RELEASE. En gros ce sont tous les zip, tar.gz etc officiels.
* public-acceptance : propose les déployables dans des versions Release Candidate (RC). Ce dépôt est présent pour des raisons historiques.


Afin de ne pas perturber les scripts existants, les URLs de ces trois dépôts sont aussi mappées sur les anciennes par un proxy.
Le mapping est le suivant : 

repo.squashtest.org/maven2/releases -> repo2.squashtest.org/repository/maven-public-releases
repo.squashtest.org/distribution -> repo2.squashtest.org/repository/public-releases
repo.squashtest.org/acceptance -> repo2.squashtest.org/repository/public-acceptance

Un commentaire étendu sur le montage est disponible à la fin de ce document (la section `Fonctionnement`).



# Configurer Nexus Public

Les actions suivantes doivent être menées : 

## Sur Nexus privé : 

- Créer un compte `nexus-public` 
- Lui créer un rôle avec les droits "browse" et "read" sur les dépôts "maven-public-releases", 
- ainsi que les droits "nx-search-read" (optionnel, n'est utilisé que par `nexus-mirroring.sh` pour l'instant inactif)

## Sur Nexus public : 

- Créer un dépôt de type proxy pour "maven-public-releases". Dans la section `Authentication`, renseigner le login/password 
créé sur le Nexus Privé.
- Créer les dépôts "public-releases" et "public-acceptance".
- Créer un compte appelé 'jenkins-ci'. IMPORTANT : le login/mdp associé devront être portés dans Vault, sous le nom de `jenkins-nexus-public-credentials`.
- Créer un rôle pour `jenkins-ci` et lui donner les droits `view-*` sur les dépôts "public-releases" et "public-acceptance".
- Créer un autre rôle `custom-anonymous` et lui donner les droits "read" et "browse" pour les trois dépôts, ainsi que le droit global "nx-search-read".
- Affecter ce rôle au compte `anonymous`.  

Si on souhaite utiliser "nexus-mirroring.sh", il faut aussi : 


- créer un compte `nexus-public`
- lui créer un rôle avec "view-*" pour les dépôts concernés par le mirroring, ainsi que "nx-search-read".
- créer une tâche cron pour automatiser l'exécution du script. Le script devra être lancé avec les credentials pour Nexus Privé 
et Nexus Public dans son environnement, via la variable CURL_CREDENTIALS (voir script). 

-------

# Fonctionnement

Pour faire court voici la configuration actuelle des dépôts : 
* maven-public-releases : c'est un dépôt de type *proxy*, qui est sourcé depuis le dépôt du même nom sur le nexus privé.
* les deux autres : ce sont des dépôts de type *hosted*, qui sont alimentés par push direct depuis la CI.


### Raisons

Pour comprendre ce choix il faut d'abord expliquer une limitation importante de Nexus, qui est que le mirroring est impossible. 
La différence entre proxy (au sens de Nexus) et mirror qui nous intéresse ici est la suivante :
* un miroir est une copie locale d'un ensemble de fichier distants. 
* un proxy ne possède pas de copie locale, mais permet d'accéder aux fichiers distants. 

En particulier, le GUI Nexus permet pas d'inspecter le contenu d'un dépôt proxy. Un élément ne sera browsable sur un dépôt proxy 
uniquement s'il a été téléchargé au moins une fois, auquel cas il est alors présent dans le cache local.  

Pour revenir à nos besoins utilisateurs, le besoin d'inspecter les fichiers via le GUI est primordial en ce qui concerne les déployables, 
mais un peu moins présent en ce qui concerne les artifacts Maven qui sont en général accédés par un client Maven et très rarement par 
des humains.

Le choix donc d'avoir des dépôts 'hosted' pour les dépôts public-releases et public-acceptance relève donc d'un compromis 
entre les besoins utilisateurs et la complexité technique : ces dépôts doivent être browsables donc nous ne pouvons pas 
les définir comme proxy (non-browsables), et ils doivent être alimentés par un double push. Le besoin de browser les artifacts, 
bien que souhaitable, n'est pas indispensable, et pour l'instant ne justifie pas la complexité des développements requis
(voir à ce sujet la fiche [https://project.squashtest.org/browse/TRAN-49] pour une discussion approfondie). 

### Alternatives 

* Une version payante de Nexus pourrait solutionner le problème du mirroring : en effet il dispose d'une fonctionnalité 
d'export/import. On pourrait en théorie dumper le dépôt sur Nexus privé, rsync sur Nexus public, puis importer, et n'utiliser 
que des dépôts de type 'hosted'.

* Artifactory propose une fonction de 'smart repository', qui permet de browser un dépôt distant depuis le GUI du dépôt proxy.
Cependant cela implique que les deux instances soient hébergées sur un Artifactory.

* Le script `nexus-mirroring.sh` ici présent permet de forcer le peuplement du cache de proxys en calculant la différence entre les 
dépôts sur Nexus Privé et sur Nexus Public puis en déclanchant les téléchargements nécessaires. Il est prévu pour des dépôts 'proxy'.
 
Le script fonctionne mais nous avons choisi de ne pas l'utiliser car l'API REST de Nexus pagine ses résultats par paquets de 10, 
non paramétrable. Dans le cas des artifacts Maven qui sont particulièrement nombreux cela impliquerait des dizaines de milliers 
de requêtes par exécution du script. Pour l'instant aucune évolution n'est prévue du côté Nexus sur ce point.    


