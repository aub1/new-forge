#! /bin/bash

# ####################################################
#
# 05/03/2021 :
# OBSOLETE : Nexus Public n'est plus un miroir
#
# Il est maintenant alimenté par push direct depuis la forge.
#
# Ce script est conservé à des fins documentaires car c'est notre
# seul exemple actuel de comment attaquer l'API Nexus et traiter
# le résultat par Bash.
#
# ####################################################


# ####################################################
#
# nexus-mirroring.sh
#
# ====================================================
#
# What is this
# ------------
#
# This script synchronize the public content exposed on Nexus Public with those hosted on the Nexus Private.
# It does so by polling the assets on Nexus Private and on Nexus Public via their REST API, compares the lists, and
# finally either triggers the download of missing assets or the deletion of the extraneous assets.
#
# Requirements
# ------------
#
# The following must be available :
# - curl
# - jq
# - the environment variable CURL_CREDENTIALS, on the forme "user:password"
#
# Optionally :
# - NEXUS_PUBLIC_URL : the base url of the Public Nexus. If undefined a default value will be assigned.
# - NEXUS_PRIVATE_URL : the base url of the Private Nexus. If undefined a default value will be assigned.
#
#
# ##############################################################
#
# Temporary file structure and Naming Conventions
# ------------------------------------------------
#
# Part of the logic of this script assumes the following naming conventions :
#
#   - Everything happens in a base temporary directory, named TMPDIR
#   - In that TMPDIR, all operations regarding the repository <repo_name> will happen in a subdirectory named <repo_name>
#   - In the subdirectory, the following files will be created :
#       * private-assets-X, which is the page X of the (paginated) list of assets on the Private Nexus for that repository
#       * public-assets-X, same thing for the Public Nexus
#       * public-missings, the file that contains the negative difference (ie the files that exists on Private Nexus
#                         that aren't present on Public Nexus)
#       * public-obsoletes, the that contains the positive difference (ie files that don't exist anymore on Private Nexus
#                         and should be removed from Public Nexus)
#
#
# Troubleshootings
# ----------------
#
# If sh*t happens, you can trace the progression of the script by looking at the files generated in its temporary
# directory (see the Naming Conventions). The temporary directory is usually '/tmp/nexus-mirroring/'.
# Also logs in the console can help you in your troubleshootings.
#
# Other Documentation
# --------------------
#
# See Nexus REST API :
#   https://help.sonatype.com/repomanager3/rest-and-integration-api/search-api
#   https://help.sonatype.com/repomanager3/rest-and-integration-api/pagination
#
# ####################################################


echo "executing nexus-mirroring.sh"

##################### SETUP ######################################

: "${NEXUS_PUBLIC_URL:=https://repo2.squashtest.org}"
: "${NEXUS_PRIVATE_URL:=https://nexus.squashtest.org/nexus}"

: "${CURL_CREDENTIALS:? CURL_CREDENTIALS is not set, aborting}"


TMPDIR="/tmp/nexus-mirroring"
rm -rf "$TMPDIR"
mkdir "$TMPDIR"
echo "temporary directory is : $TMPDIR"

################# FUNCTIONS #####################################

function log {
  echo -e "$1"
}

function perror {
  echo -e "$1" >&2
}



# Gather all the assets one a given Nexus instance and repo name.
# All the (paginated) assets will be printed as files in the tmp directory.
# The files are created according to the naming convention described at the
# top of this file.
#
# Args :
# - $1: the name of the repository
# - $2: either "public" or "private"
function crawl_assets {

  # setup
  local repo_name="$1"
  local nexus_instance="$2"

  if [ "$nexus_instance" = "private" ]
  then
      local nx_url=$NEXUS_PRIVATE_URL
      local credentials_opt="-u $CURL_CREDENTIALS"
  else
      local nx_url=$NEXUS_PUBLIC_URL
      local credentials_opt=""
  fi

  log "fetching assets inventory from instance '$nexus_instance' and repository '$repo_name'"

  #########
  
  local filename_tpl="${TMPDIR}/${repo_name}/${nexus_instance}-assets"
  local token=""
  local base_url="${nx_url}/service/rest/v1/search?repository=${repo_name}"
  local cnt=0
  
  while [ "$token" != "null" ]
  do
    local output_file="${filename_tpl}-${cnt}"
    
    # Note : the query parameter 'continuationToken' must be set only if the token exists, because Nexus would reject the request otherwise
    local url=${base_url}${token:+"&continuationToken=$token"}

    log "\t fetching page : ${cnt}"

    curl $credentials_opt -s -o "$output_file" "$url"  
    if [ "$?" -ne 0 ]
    then
        perror "request failed, diagnosing request : $url"
        curl $credentials_opt -v "$url"
        exit 1
    fi


    # Note : the token must be stripped from its surrounding quotes, otherwise Nexus would complain that the request is malformed
    token=$(cat "$output_file" | jq '.continuationToken' | sed s/\"//g)
    cnt=$((cnt+1))

  done


}


# The function calculates the difference between public et private for a given repo.
# Args :
# - $1 : the repository name
function difference {

  log "calculating repository difference for repo : $1"

  pushd "${TMPDIR}/${1}" > /dev/null

  # Collect the assets path
  cat private-assets-* | jq '.items[].assets[].path' | grep -E '(jar|pom|tar.gz|zip)"' | sed s/\"//g | sort  > private-allassets
  cat public-assets-* | jq '.items[].assets[].path' | grep -E '(jar|pom|tar.gz|zip)"' | sed s/\"//g | sort  > public-allassets

  # Calculate the difference
  diff --new-line-format="" --unchanged-line-format=""  public-allassets private-allassets > public-obsoletes
  diff --new-line-format="" --unchanged-line-format=""  private-allassets public-allassets > public-missings

  popd > /dev/null
}


# Will instruct the Public Nexus to pull the missing assets by
# querying the repository and let the proxy-repository mechanic do its work.
# Args :
# - $1 : repository name
function pull_missing {
  log "pulling missing assets for repo : ${1}"

  for asset in $(cat "${TMPDIR}/${1}/public-missings")
  do
    local url="${NEXUS_PUBLIC_URL}/repository/${1}/${asset}"
    log "\t pulling asset : $asset"
    curl -s "$url" -o /dev/null
    if [ "$?" -ne 0 ]
    then
      log "Error while pulling asset, debugging curl request ${url} : "
      curl "$url" -v -o /dev/null
      exit 1
    fi
  done
}


# Args :
# -$1 : the name of the repository
function remove_obsolete {

  log "removing obsolete assets for repo : $1"

  pushd "${TMPDIR}/${1}" > /dev/null

  # Here we collect the asset ID of the obsolete assets on the Public Nexus
  # Note : here we opt for a grep-based implementation, but the awk-based implementation looks promising too :
  # https://stackoverflow.com/questions/19380925/grep-a-large-list-against-a-large-file

  # Note : in the assets files the field 'id' is next to the field 'path' on which we are looking for matches,
  # that's why we use the -A 1. We enforce that particular field order by reformatting the files with jq first.
  # In short, we filter the asset files and retain 'path' and 'id', then we filter by the content of public-obsoletes,
  # and then we filter to retain 'id' only. We then only have to clean the data with sed.

  cat public-assets-* | \
    jq '.items[].assets[] | { "path" : .path, "id" : .id }' | \
    grep '"path"' -A 1 | \
    grep -F  -f public-obsoletes -A 1 | \
    grep '"id"' | \
    sed 's/.*: "\(.*\)",\?/\1/g' > public-obsoletes-ids


  while read obsolete_id
  do
    echo "\t deleting asset : $obsolete_id"

    local url="${NEXUS_PUBLIC_URL}/service/rest/v1/assets/${obsolete_id}"
    curl -s -u "$CURL_CREDENTIALS" -X DELETE "$url"
    if [ "$?" -ne 0 ]
    then
        local failed_asset_path=$(cat public-assets-* | \
            jq ".items[].assets[] | select (.id == \"${obsolete_id}\" ) | .path")

        log "error while deleting asset ${obsolete_id} of path ${failed_asset_path}"
        log "debugging : "
        curl -v -u "$CURL_CREDENTIALS" -X DELETE $url
        exit 1
    fi
  done < public-obsoletes-ids


  popd > /dev/null

}

####################################### MAIN ################################



# Argument : 
# $1 : the repository name
function synchronize {

  log "\n--------------\nSynchronizing content for repo : $1 \n "

  local repo_name="$1"
  mkdir "${TMPDIR}/${repo_name}"

  crawl_assets "$repo_name" "private"
  crawl_assets "$repo_name" "public"

  difference "$repo_name"

  pull_missing "$repo_name"
  remove_obsolete "$repo_name"

  log "\nSynchronization complete"
}


synchronize "maven-public-releases"
synchronize "public-releases"
synchronize "public-acceptance"



