
* Lister les plugins Jenkins par leur noms techniques :

    curl -L https://updates.jenkins.io/update-center.json | head -n 2 | tail -n 1 | jq | less

* Retrouver le mot de passe Jenkins : 

    kubectl -n jenkins get secret jenkins -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode; echo

    