# Combinaison de podTemplate dans une pipeline

## Méthode de l'héritage

L'une des manières pour combiner plusieurs podtemplate est de passer par l'héritage.

Dans le code ci-dessous, on déclare dans le script d'une pipeline un podtemplate avec le label "MyFinalPod" 
qui va hériter de 2 autres podtemplates : mvn-pod-template et gradle-pod-template.

Chaque podtemplate contient un container avec respectivement Maven et Gradle d'installé.

Il est également possible de définir ce podtemplate (et son héritage) dans la section dédiée sur Jenkins 
(en passant par la GUI) et de juste appeler le nom du podtemplate comme lors d'une utilisation standard.

```
podTemplate (label: 'myFinalPod', inheritFrom: 'mvn-pod-template gradle-pod-template', yamlMergetrategy: merge()) {
    node('myFinalPod') {
        stage('Main') {
            container(name: 'mvn-builder', shell: '/bin/bash') {
                sh """
                mvn -version
                """
            }
            container(name: 'gradle-builder', shell: '/bin/bash') {
                sh """
                gradle --version
                """
            }
        }
    }
}
```

Le paramètre `inheritFrom` peut contenir un ou plusieurs podtemplate, avec chaque élément séparé par un espace.

Le paramètre `yamlMergeStrategy` est par défaut en override(), afin de garder l'ensemble des éléments il 
faut indiquer la méthode merge().
 
On exécute ensuite des commandes dans les containers provenant de chaque podtemplate.

Ceci représente un cas basique, des incertitudes subsistent sur le comportement de l'héritage, notamment pour 
des pods plus complexes.