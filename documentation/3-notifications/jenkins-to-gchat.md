# Notification des résultats des builds sur Google Chat

Le plugin Jenkins "Google Chat Notification Plugin" permet d'envoyer des notifications sur Google Chat pendant (ou à la suite) des différents builds.

L'envoi de la notification se fait via une commande dans le script d'une pipeline (également disponible en Post Action d'un job Freestyle).

La destination se fait en indiquant directement l'url du webhook ou en passant par les credentials Jenkins (notamment par l'id du secret).

On a à ce jour 4 canaux dédiés :

Nom du canal           | ID

TF Build Status        | tf-build-status

TM Build Status        | tm-build-status

TF Legacy Build Status | tf-legacy-build-status

Default Build Status   | default-build-status

## Utilisation

### Sans stdlib 

La ligne à injecter est la suivante (compactée pour une meilleure lisibilité): 

```
googlechatnotification message: "${env.JOB_NAME} #${env.BUILD_NUMBER} ${currentBuild.currentResult} : ${env.BUILD_URL}", 
                       [notifySuccess: true,] 
                       notifyBackToNormal: true, 
                       notifyFailure: true, 
                       notifyUnstable: true, 
                       sameThreadNotification: true, 
                       url: "id:my-credential-id"
```

Plusieurs paramètres sont disponibles pour spécifier le comportement en fonction du résultat du build.

Dans notre cas, on souhaite principalement notifier les builds en échec ou instable, ainsi que les succès suite à un échec.

On souhaite également notifier les succès des releases.

Le paramètre "url" peut correspondre soit à un webhook, soit à un credential Jenkins (spécifié avec id:credential-id).

La méthode privilégiée est bien entendu celle avec le credential Jenkins.

### Avec stdlib

Pour un job utilisant la stdlib, il suffit d'ajouter dans la LinkedHashMap une clé "gchat_credentials" et en valeur l'id du credential Jenkins concernant le canal souhaité.

Si cette paire clé/valeur n'est pas spécifiée, alors la notification sera tout de même présente avec un envoi sur "Default Build Status"

Exemple : 

```
stdbuilds([

	'releaseProfiles' : [],
	'buildProfiles' : ['buildProfile1','buildProfile2'],
	'gchat_credentials' : 'tf-build-status'

])
```

## Installation et configuration

### Google Chat

La principale configuration à faire côté Google Chat consiste à paramétrer un webhook pour que Jenkins puisse envoyer les informations.

La procédure est la suivante :

1. S'il faut un nouveau salon, le créer via le bouton associé.

2. Accéder au menu "Gérer les webhooks" dans les détails du salon

3. Cliquer sur le bouton "Ajouter un autre"

4. Renseigner le nom du webhook, par défaut le nom utilisé est jenkins-hook.

### Jenkins

Côté Jenkins, il suffit d'installer le plugin Google Chat Notification.

Le reste de la configuration se fait via Vault avec les external secrets et via la pipeline.

### Vault et External Secrets

Les webhooks des salons Google Chat sont stockés dans Vault.

Ceux-ci sont ensuite injectés dans Jenkins grâce aux external secrets en tant que credential.

Ils peuvent ensuite être exploité par les pipelines.

La procédure est la même que pour l'intégration d'un secret :

1 : Se connecter à Vault

2 : Injecter le secret, notamment dans secretkv/jenkins/${webhook_name} avec pour clé "url".

3 : Créer un external secret au format suivant puis l'appliquer

```

apiVersion: kubernetes-client.io/v1
kind: ExternalSecret
metadata:
  name: channel-name
  namespace: jenkins
spec:
  backendType: vault
  data:
    - key: secretkv/data/jenkins/channel-name
      name: text
      property: url
  template:
    metadata:
      annotations:
        jenkins.io/credentials-description: Webhook de Channel Name
      labels:
        jenkins.io/credentials-type: secretText
        velero.io/exclude-from-backup: "true"
      name: channel-name
  vaultMountPoint: kubernetes
  vaultRole: extsecret

```

4 : Le secret doit être créé et accessible via Jenkins.