# Notification des résultats des builds sur Mattermost

## Utilisation

Le plugin Jenkins "Mattermost Notification Plugin" permet d'envoyer des notifications sur Mattermost pendant (ou à la suite) des différents builds.

Ceci se fait via le script de la pipeline en appliquant la fonction suivante : 

```
mattermostSend(color: "#FF0000", channel: "tf-build-status", message: "${currentBuild.fullDisplayName} ${currentBuild.currentResult} : ${env.BUILD_URL}")
```

A priori on souhaite indiquer principalement les échecs des builds et les succès des releases.

Ceci se fait principalement dans la section "post" d'une pipeline (même si on peut le faire n'importe où).

Exemple : 

```
post {
    unsuccessful {
        mattermostSend(color: "#FF0000", channel: "tf-build-status", message: "${currentBuild.fullDisplayName} ${currentBuild.currentResult} : ${env.BUILD_URL}")
    }
}
```

Les principales variables sont la couleur et le canal.

A ce jour, 3 canaux sont disponibles : tf-build-status, tm-build-status et squash-tf1---legacy

L'adresse du webhook n'a pas besoin d'être définie dans la fonction car elle est dans la configuration par défaut du plugin, 
dans Manage Jenkins -> Configure System -> Global Mattermost Notifier Settings -> Endpoint

## Installation et configuration

### Mattermost

La principale configuration à faire côté Mattermost consiste à paramétrer un webhook pour que Jenkins puisse envoyer les informations.

Attention cependant car le créateur du webhook doit être présent dans le canal de discussion pour que Jenkins puisse communiquer dessus. 
Autrement, l'envoi se terminee en échec avec un Forbidden.

Note : Un utilisateur indépendant sera créé par la suite pour qu'il soit à l'origine de tous les webhooks. Doc à mettre à jour quand il sera créé.

La procédure est la suivante :

1. Accéder au menu Intégration. Des droits sont nécessaires pour avoir cette option.

2. Accéder au menu "Incoming Webhooks"

3. Cliquer sur le bouton "Add Incoming Webhook"

4. Remplir les différents champs du formulaire avec les informations souhaitées

5. Ajouter l'utilisateur à tous les canaux concernés

### Jenkins

Côté Jenkins, il suffit d'installer le plugin et de le configurer.

La procédure est la suivante :

1. Installer le plugin Mattermost Notification Plugin

2. Aller dans le menu Manage Jenkins

3. Accéder au menu Configure System

4. Dans la section Global Mattermost Notifier Settings, indiquer l'adresse du webhook configuré précédemment.

