# Configurer un node pour un noeud teinté

Certains noeuds sont teintés. Pour les faire admettre sur les noeuds correspondants vous pouvez rajouter des `toleration` 
sur vos pods, mais attention les abus finiront par être repérés !

## Tainted Nodes

* forge-kube-master :

Normalement, n'accepte que les daemonsets et les pods légitimes à être planifiés sur le controlplane. 
Si vous pensez que c'est votre cas, rajouter au pod la toleration suivante :

    tolerations:
    - key: "node-role.kubernetes.io/master"
      operator: "Exists"
      effect: "NoSchedule"
      
* forge-worker-3 :

Normalement, n'accepte que les daemonsets et les pods qui ont à voir avec les builds de Squash-TM.
Si vous pensez que c'est votre cas, rajouter au pod la toleration suivante :

    tolerations:
    - key: "node-role.henix.com/dedicated-node"
      operator: "Exists"
      effect: "NoSchedule" 