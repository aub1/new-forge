# Contenu de l'environnement

Afin d'exécuter une ou plusieurs étapes de build il est nécessaire de rendre disponible dans l'environnement plusieurs 
ressources : config, secrets, variables d'environnements etc.

Ici nous listons les ressources importantes, à quels endroits elles sont définies, et de quelle façon elles sont injectées
dans un pod de build Jenkins. 

Note : la table ci-dessous rend beaucoup mieux rendue en tant que Markdown, donc pour une fois ne faites pas 
que lire le raw text !


Ressource                       |   Description                     |  Défini dans      |   Mode d'injection    |
--------------------------------|:---------------------------------:|:-----------------:|:---------------------:|
jenkins/docker-registry | credentials pour les images docker | Kubernetes | Par le pod template   |
jenkins/jenkins-bitbucket-credentials-ssh | clé ssh pour bitbucket  | Kubernetes | Build step Jenkins 'sshagent' |
jenkins/jenkins-bitbucket-credentials-lmdp | login/pwd pour bitbucket | Kubernetes | Build step Jenkins 'withCredentials' |
config global git | username et email du committeur quand c'est Jenkins | Jenkins global conf | automatiquement géré par Jenkins
jenkins/jenkins-nexus-credentials | login/pwd pour Nexus  | Kubernetes | Par le pod template, sous forme de variables NEXUS_USERNAME et NEXUS_PASSWORD
jenkins/jenkins-nexus-public-credentials | login/pwd pour Nexus Public  | Kubernetes | Par le pod template, sous forme de variables NEXUS_PUBLIC_USERNAME et NEXUS_PUBLIC_PASSWORD
jenkins/sonar-token | Token d'identification au Sonar | Kubernetes | Automatiquement géré par Jenkins, sous forme de variables comprises par mvn sonar:sonar
jenkins/squashtf-token | Token de l'orchestrateur Squash de prod | Kubernetes | Build step Jenkins 'withCredentials' |
jenkins/agents-deploy-robot | kubeconfig pour le ns agents | Kubernetes | Build step Jenkins 'withCredentials' |
jenkins/squash-deploy-robot | kubeconfig pour le ns squash | Kubernetes | Build step Jenkins 'withCredentials' |



Note : un secret noté 'Défini dans Kubernetes' signifie en réalité 'Dans Vault puis importé dans Kubernetes puis 
importé dans Jenkins via le Kubernetes Credentials Provider Plugin'. Après tout un devops reste un plombier 
avant tout !    

