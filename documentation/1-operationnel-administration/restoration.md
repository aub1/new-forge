# Restauration

## Restauration depuis un backup Velero

Cette section part du principe qu'un client Velero est déjà présent sur la machine.

Pour restaurer un backup déjà créé par Velero, exécuter la commande suivante :

`velero restore create --from-schedule velero-jenkins-schedule`

On peut ensuite consulter son avancement avec la commande describe.

`velero restore describe ${VELERO_SCHEDULE_RESTORE_NAME}`

## Récupération des identifiants sur Keepass

Le Keepass est ensuite nécessaire pour unseal le Vault. Il faut donc récupérer la base Keepass depuis le Google Drive. 

Cette base est au format .kdbx et est disponible dans le Drive des administrateurs du Kube.

Il faut ensuite demander aux administrateurs système l'accès à la master key pour ouvrir la base récupérée.

## Unseal du Vault et mise à jour de sa configuration

1 : Se connecter au container Vault pour réaliser les opérations.

2 : Débloquer le Vault après le redémarrage. Pour cela il faut utiliser la commande `vault operator unseal` et 
renseigner 3 des 5 unseal keys présente dans le Keepass.

3 : Se connecter à Vault avec la méthode souhaitée :

  * Root token : `vault login`
  * Utilisateur : `vault login -method=userpass username=${USERNAME}`

Par défaut le root token est révoqué. Il faut principalement utiliser la connexion via userpass.

4 : Mettre à jour la configuration de l'authentification Kubernetes avec la commande suivante :

```
vault write auth/kubernetes/config \
    token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
    kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443" \
    kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
```

5 : Il est ensuite possible de vérifier que le tout fonctionne grâce aux ExternalSecrets. Si le statut est en SUCCESS,
alors le secret est de nouveau généré et disponible.

`kubectl get -n ${EXTERNAL_SECRET_NAMESPACE} externalsecrets.kubernetes-client.io ${EXTERNAL_SECRET_NAME}`

6 : Ne pas oublier de se déconnecter en supprimant le fichier ~/.vault-token dans le container Vault.