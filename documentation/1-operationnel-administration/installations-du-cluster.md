## Installation du cluster 

Les étapes nécessaires à suivre afin d'installer et configurer le cluster de new-forge se font à l'aide de ses documentations correspondantes:

#### 0. Installation de k3s : 
		[install.md](../../cluster-global/k3s/install.md)

#### 1. Configuration de storage : 
		[config-volumes.md](../../cluster-global/storage/config-volumes.md)

#### 2. Configuration des backups avec Velero : 
		[usage.md](../../velero/usage.md)

#### 3. Mise en place la gestion des secrets Vault : 
		[secret.md](../../cluster-global/vault/secret.md)

#### 4. Installation de Jenkins : 
		[readme.md](../../jenkins/helm/readme.md)
		Configuration du plugin Vault dans Jenkins (si besoin, actuellement il n'est pas installé):
			[secret.md](../../cluster-global/vault/secret.md)

#### 5. Mise en place de concentration des logs du cluster :
		[how-to.md](../../cluster-global/logging/how-to.md)
		
#### 6. Mise en place de monitoring du cluster :
		[how-to.md](../../cluster-global/logging/how-to.md)