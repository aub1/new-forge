DRAFT 0.1


# Cadre général de la maintenance de la CI

Le but de ce document n'est pas de présenter des opérations techniques en détails mais d'expliciter les grandes lignes 
d'une opération de maintenance, leur domaine, leur chronologie et les interactions entre l'équipe Transverse et l'équipe 
SysOps. 


## Aperçu

En gros : 
* On définit le domaine Fonctionnel et le domaine Infra
* Le domaine Fonctionnel porte les services (ce qui inclus K3S lui-même), le domaine Infra porte sur les VMs
* Le domaine Fonctionnel est à charge de l'équipe Transverse
* Le domaine Infra est à charge des SysOps


## Domaines

### Domaine Fonctionnel

Consiste en :
* Le cluster K3S
* Tous les services administrés par K3S (notamment Jenkins mais aussi Vault etc)
* Le Nexus Privé, Nexus Public et SonarQube (qui sont géré à l'extérieur du cluster)

Les mainteneurs sont l'Equipe Transverse, et ponctuellement les leads TF.


### Domaine Infra

Consiste en :
* l'hyperviseur nx-dev06
* les VMs 10.0.0.200, 201 et 202
* les containers (au sens ProxMox) des Nexus Privés, Nexus Public et SonarQube

Les mainteneurs sont les SysOps.


## Plan de maintenance

### Maintenance du domaine Fonctionnel

Ici la maintenance parle principalement des binaires des applis et de leur plugins. La maintenance au sens de configuration
n'est pas le sujet ici.

*Maintenance ponctuelle*

Lorsqu'une alerte sécurité est remontée par un service, la brique en défaut doit être remplacée.

*Maintenance périodique*

Périodiquement les plugins seront montés de version inconditionnellement. Les périodes dépendent du service : par exemple 
Nexus est assez stable dans son cycle de vie et peut être mis à jour une fois par an, alors que Jenkins a une importante grappe 
de plugins maintenus à des rythmes différents et demande donc une période plus courte pour les MAJ.

**TODO** Proposition de périodes : 

Service         | Période      |
---------------:|:-------------|
Jenkins         | tous les 3 mois
Tous les autres | tous les ans


### Maintenance du domaine Infra

**TODO** : Je ne peux pas ici décider de la politique de maintenance choisie pour les SysOps, mais une fois qu'elle est connue
penser à la reporter ici.

 
## Procédure

**TODO** valider le processus suivant.

## phase 1 : planifier

En premier lieu on décide de si les SysOps sont impliqués dans l'opération : ça sera le cas dans deux situations : 
* l'opération porte sur le domaine Infra, 
* la phase backup demande une opération SysOps (voir phase 3).

Que ce soit au niveau infra ou au niveau service, il faut prendre en compte que cela peut occasionner des interruptions de service.
Ce n'est pas toujours le cas techniquement, cela dit pour simplifier nous dirons que "Maintenance = Interruption de service". 
L'interruption de service ne sera pas renforcée par une vraie coupure d'accès : une simple notification aux 
utilisateurs suffira. 

Cette notification précise les services qui seront en maintenance ainsi que les dates/heures de l'intervention.
La notification est émise sur Mattermost sur le canal des utilisateurs de la Forge, et au plus tôt. 
L'équipe Transverse est responsable de la notification.


## phase 2 : backup

Une opération maintenance nécessite un backup préalable. Le backup peut se faire de deux façons :

cas 1 : si la cible de la maintenance est sous backup Velero -> utiliser le backup Velero et les Transverses sont donc 
autonomes.
cas 2 : dans les autres cas il est prudent de faire un backup du VM/container, et les SysOps sont donc impliqués.  

## phase 3 : effectuer l'opération

C'est l'installation de plugins, packages debian etc proprement dite.

## phase 4 : validation

On considère que l'opération de maintenance est un succès si les *services* redémarrent normalement et le cas échéant qu'aucune alerte
n'est remonté par le dit service.
Dans le cas de Kubernetes (service ou VM), ne pas oublier de déverouiller Vault si nécessaire. 
En cas de problèmes on se débrouille, éventuellement on peut décider d'abandonner et de restaurer le backup. La restauration
est effectuée par la même équipe qui s'est chargée de créer le backup (Transverses ou Syspos).

## phase 5 : ouverture des services

L'équipe Transverse annonce sur Mattermost que les services sont disponibles.


  


