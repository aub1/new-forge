# Créer des utilisateurs

On suit globalement la procédure indiquée ici : 

    https://medium.com/better-programming/k8s-tips-give-access-to-your-clusterwith-a-client-certificate-dfb3b71a76fe

# MAIS
La procédure de création telle que décrite ici par exemple ([1]) est globalement correcte. Cependant il y a un lol :
le ticket [2] nous dit que la commande `kubectl certificate approve <csr>` va signer la csr avec le mauvais certificat 
(au moins dans k3s). 

    * rapatrier la csr de l'utilisateur sur le noeud maître, 
    * signer avec 
        ```shell script
        openssl x509 -req -in cert-req.csr \
          -CA /var/lib/rancher/k3s/server/tls/client-ca.crt \
          -CAkey /var/lib/rancher/k3s/server/tls/client-ca.key \
          -CAcreateserial \
          -out certif.crt -days 3650
        ```
On peut ensuite rapatrier le certificat signé pour que l'utilisateur puisse l'inclure dans le kubeconfig.
 
Attention : Le .key et le .crt récupéré (respectivement les champs client-key-data et client-certificate-data) doivent 
être inclus dans le kubeconfig en base64 en utilisant par exemple la commande suivante : `cat file | base64 -w 0`
 
On peut ensuite créer des permissions. Pour cela se référer à `cluster-global/roles/readme.md`

