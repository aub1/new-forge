# Migration des projets Squash TM

## Maven sur des projets simples (sans version spécifique modifiée de maven, de modules complexes, etc.)

1 : Modification du pom.xml pour pointer sur les bons dépôts dans distributionManagement.

```
<distributionManagement>
    <repository>
      <id>squash-release-deploy-repo</id>
      <name>Squash releases deployment repo</name>
      <url>${deploy-repo.release.url}</url>
    </repository>
    <snapshotRepository>
      <id>squash-snapshot-deploy-repo</id>
      <name>Squash snapshot deployment repo</name>
      <url>${deploy-repo.snapshot.url}</url>
    </snapshotRepository>
</distributionManagement>
```

2 : Identifier sur l'ancienne forge la ligne de commande Maven pour pouvoir inclure 
les différents paramètres dans l'appel Maven de la pipeline. 
Le point difficile est la récupération des profils : certains profils activés sont définis 
directement dans le `pom.xml`, d'autres sont en fait définis dans les fichiers de settings 
fourni à Maven par le job. En général on veut récupérer les premiers (ceux du `pom.xml`), 
mais pas les seconds.

3 : Créer un Jenkinsfile à la racine du projet avec le script suivant 
(tout en ajoutant les paramètres identifié dans l'étape 2) :

  ```
pipeline {
    agent {
        kubernetes {
            label 'maven-builder-jdk8'
            defaultContainer 'maven'
        }
    }

    environment {
        JAVA_TOOL_OPTIONS = "-Duser.timezone=Europe/Paris"
        HOME = "${WORKSPACE}"
    }

    stages {
        stage('Build') {
            steps {
                sh "mvn -B -ntp deploy -Pci"
            }
        }
    }
}
  ```

Note : le profile `ci` est normalement automatiquement activé, on le rajoute explicitement 
à titre de documentation. Ce profile peut être desactivé (même si c'est rarement souhaitable)
en passant la property `-DdisableITProfile`.
On peut aussi le cas échéant passer en argument les autres profiles et properties relevées à la phase 2.


4 : Push

5 : Création d'un job de type pipeline sur la nouvelle forge en spécifiant les éléments suivants :

  * Cocher `Build when a change is pushed to Bitbucket`
  
  * En définition de pipeline mettre `Pipeline script from SCM`
  
  * Sélectionner Git pour le SCM et indiquer l'URL ainsi que les identifiants
  
  * Dans `Additional Behaviours` ajouter `Check out to specific local branch` avec pour valeur `master`

6 : Lancer le build

7 : Vérifier que ce soit bon à la fois sur l'ancienne et sur la nouvelle forge.

8 : Configurer le webhook sur le dépôt Bitbucket.
Dans `Repository settings`, aller dans `Webhooks`, `Add webhook` et indiquer l'URL https://{new_forge_url}/bitbucket-hook/ 
à l'image de ce qui était présent pour l'ancienne forge.

## Troubleshooting :

1 : Accès aux artéfacts de repo.squashtest.org.

En l'état repo.squashtest.org n'est pas géré par Nexus, ce qui peut causer des soucis 
avec le build lorsqu'il doit chercher des dépendances.
Il est possible de contourner ce soucis de deux manières :
  * Build les projets localement et les déployer sur le Nexus.
  * Injecter les artéfacts manuellement via la GUI de Nexus (en les récupérant depuis repo.squashtest.org).

2 : Cannot access ${deploy-repo.snapshot.url} using the registered transporter factories: WagonTransporterFactory: Unsupported transport protocol

Cette erreur survient quand, en adaptant le projet pour fonctionner sur la nouvelle forge, on rend le `pom.xml` incompatible 
pour son build sur l'ancienne forge. C'est problématique car dans un premier temps on souhaite maintenir les projets sur 
l'ancienne forge tant qu'on aura pas quelque chose de solide et facilement exploitable à proposer aux devs à la place.

Ici l'erreur dit en particulier que la property `${deploy-repo.snapshot.url}` n'est pas définie. Ca arrive si le `pom.xml`, 
avant sa migration, déviait du standard et définissait autrement son `snapshotRepository`. Pour contourner ce souci, 
il est possible d'ajouter le fichier de configuration `global-jira-sync-settings` en tant que fichier de conf maven global 
et d'activer le profil `jenkins-env-properties`. 
Pensez à propager cette modification sur l'invocation de Maven principale, mais aussi dans la section `Maven Release` !