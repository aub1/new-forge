# Nettoyage de l'espace disque sur les Workers

Les principales fuites d'espace disque sont :
* la piste d'audit de Vault, 
* les images Docker orphelines produites lors des builds CI/CD


## Nettoyage de la piste d'audit Vault

1. Tunnel sur Vault :

    kubectl -n vault exec -it vault-0 -- sh
    
2. RAZ du fichier de log : 

    cd /vault/audit
    echo '' > audit.log

## Nettoyage des images docker orphelines

nous parlons ici des images contenues dans la docker registry locale des deux workers. L'opération doit être répétée 
sur chaque note.

1. Tunnel sur Worker 1

Il faut d'abord passer par le noeud master.

    ssh <user>@nx-dev03.henix.com -p22281
    ssh <user>@10.0.0.201 (ou 202)
    
Ensuite on peut exécuter les commandes suivantes :

    # retire uniquement les layers orphelines
    docker rmi $(docker images -qf 'dangling=true') 
    # supprimer toutes les images orphelines et non utilisées par un container (radical) 
    docker image prune -a 