# Procédures de l'environnement d'exécution windows

## Agent Squash orchestrator

### Démarrage
1. Se connecter sur le compte tf-agent par RDP
2. Ouvrir le dossier `C:\Utilisateurs\tf-agent\Documents\tf-agent`
3. Double-cliquer sur le script ``startup.cmd``
![Dossier tf-agent](img/dossierTfAgent.png "Dossier tf-agent")
Une fenêtre de commande apparaît, une fois l'agent enregistré il affiche un message de confirmation et la fenêtre reste ouverte.
![Agent démarré](img/agentStarted.png "Agent démarré")

### Arrêt
1. Se connecter sur le compte tf-agent par RDP
2. Cliquer sur la fenêtre de commande dans laquelle s'exécute l'agent
3. Appuyer sur la combinaison `Ctrl-C`
4. Si le premier appui ne suffit pas (en fonction de l'état exact de l'agent) renouveler l'appui sur `Ctrl-C`
![Arrêt de l'agent](img/stopAgent.png "Arrêt de l'agent")
5. Confirmer que le programme doit être terminé.