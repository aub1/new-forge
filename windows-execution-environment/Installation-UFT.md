# Windows test execution environment (UFT)

Cette machine virtuelle est dédiée à l'exécution des tests sous windows sous pilotage de l'orchestrator.

En date du 24/03/2021 elle est réservée à l'orchestrateur Squash de l'instance "graphic-windows", en attente d'un mécanisme permettant
de partager la machine entre plusieurs orchestrateur **à tour de rôle** (afin d'éviter les conflits au niveau de la session graphique).

## Installation de l'hôte
L'hôte est une VM windows, avec les besoins suivants : être accessible par RDP depuis l'extérieur (permet d'activer la couche graphique)

Spécifications :

    Processeur : 2 cœurs (dixit UFT)
    Mémoire RAM : 4 Go.
    Disque : ce qu'il faut à windows + 30 Go de marge pour installer les composants et pouvoir travailler
    OS : windows 10
    Firefox installé

De plus, elle doit pouvoir accéder aux instances du kube en HTTPS.

## Installation logicielle
Effectuer cet installation en étant connecté en tant qu'administrateur

### Mise en place de l'agent

1. Exécuter la procédure commune de mise en place de l'agent détaillée [ici](InstallationBaseEnvironnement.md)
1. Dans le fichier `startup.cmd`, modifier la définition de la variable `TAGS` comme suit :

	set TAGS=windows,uft

### Navigateurs

Exécuter la procédure commune de mise en place des navigateurs détaillée [ici](InstallationNavigateurs.md)

### Outil UFT

1. Télécharger les installeur pré-requis
Ce téléchargement se fait depuis la page https://pdapi-web-pro.microfocus.com/mysoftware/download/downloadCenter (il faut s'identifier en tant que client)
	* UFT One 15.0.2 Package For the Web Bundle (extension Web d'UFT One)
	
1. Exécuter l'installeur `UFT_One_15.0.2_Setup.exe` en tant qu'administrateur. ![SetupAsAdmin](img/uft_execute_setup_as_admin.png "Executer setup en tant qu'admin")
	1. Accepter le premier écran en cliquant sur "Next".
	![UFT_setup_1](img/uft_setup_1.png)
	![UFT_setup_1](img/uft_setup_2.png)
	2. Accepter l'installation des packages tiers en cliquant sur "OK"
	![UFT_setup_1](img/uft_setup_3.png)
	3. L'installeur proprement dit apparaît, lancer l'installation en cliquant sur "Suivant" (disponible après quelques opérations préliminaires)
	![UFT_setup_1](img/uft_setup_4.png)
	4. Sur l'écran de paramétrage initial, décocher "Participer au programme d'amélioration d'Unified Functional Testing", puis cocher "J'accepte les termes du contrat de licence".
	![UFT_setup_1](img/uft_setup_5.png)
	5. Cliquer sur "Suivant"
	![UFT_setup_1](img/uft_setup_6.png)
	6. Sélectionner les paquets suivants :

	![UFT_setup_1](img/uft_setup_composants_1.png)
	![UFT_setup_1](img/uft_setup_composants_2.png)
	![UFT_setup_1](img/uft_setup_composants_3.png)
	![UFT_setup_1](img/uft_setup_composants_4.png)
	
	Cliquer sur "Suivant"

	7. Cocher "Configurer les paramètres d'Internet Explorer", seulement.
	![UFT_setup_1](img/uft_setup_7.png)
	8. Cliquer sur "Installer"
	
1. Se reconnecter en tant que `tf-agent` et mettre en place les paramètres
   1. Ouvrir Edge et accepter l'ajout de l'extension UFT.
   1. Paramétrer la gestion des rapports
      * Ouvrir le gestionnaire de paramètres "Runtime Engine Parameters"
      ![tf-agent UFT setup](img/uft_open_runtime_parm_setup.png "Open parm setup")
      * Sélectionner Run > Report Format = RRV et confirmer.
      ![tf-agent UFT setup](img/uft_agent_parms_setup.png "Open parm setup")
   1. Mettre en place les droits réseau pour le UFT
      * Démarrer l'agent
      * lancer un PEAC UFT
      * Lors de l'apparition de la pop-up système, autoriser UFT accéder aux réseaux privés et publics.
        ![Donner les autorisations réseau java](img/acceptJavaNetworkFirstAgilitest.png "Donner les autorisations réseau java")

1. Pour pouvoir contacter le serveur de licences, déclarer dans le fichier `C:\Windows\System32\drivers/etc/hosts` le mapping suivant:

`10.0.0.201	autopass.new-jenkkins.squashtest.org`

(cette opération doit ête effectuée en tant qu'administrateur)
