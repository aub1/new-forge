# Procédure de mise en place des outils Robot Framework et Cypress en environnement d'exécution Windows

## Prérequis

Ce document part du principe que la procédure InstallationBaseEnvironnement.md a été appliquée et qu'on a Python
déjà disponible dans l'environnement.

## Installation de Robot Framework

1. Se connecter en tant qu'administrateur

2. Lancer une invite de commandes en tant qu'administrateur

3. Exécuter les commandes suivantes :

```
pip install robotframework
pip install squash-tf-services
pip install allure-robotframework
```

## Installation de Cypress

1. Se connecter sur l'utilisateur tf-agent

2. Aller sur le site officiel de node.js et récupérer l'installeur Windows x64.

3. Lancer l'installeur (et valider l'accès admin) puis poursuivre l'installation.

4. Lancer ensuite une invite de commande et installer Cypress

```
npm install cypress
```