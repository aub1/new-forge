# Installation des navigateurs

Les navigateurs sont un besoin transversal pour tous les tests d'interface Web (Agilitest, UFT, selenium...)

## Edge
Ce navigateur est nativement présent sur Windows10

## Google Chrome
1. Télécharger l'installeur depuis https://www.google.com/chrome/
2. Lancer en tant qu'administrateur l'installeur fourni
![Lancer l'installeur](img/downloadingChrome.png "Installation réseau de Google Chrome")
![Lancer l'installeur2](img/chromeFirstStart.png "Première exécution de Google Chrome")
3. Quand le navigateur se lance, le quitter sans configurer.
4. Basculez dans le compte de service et lancez Chrome
![Lancer l'installeur2](img/chromeFirstStart.png "Première exécution de Google Chrome")
5. Passez tous les écrans de configuration
6	. Au dernier écran, cliquez sur "non merci"
![Chrome NW non merci](img/chromeNWnoThanks.png "Chrome synchronisation non merci")
![Chrome NW non merci](img/chromeProfileCfgDone.png "Profil Chrome configuré")
