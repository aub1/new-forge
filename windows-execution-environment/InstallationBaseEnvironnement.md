# Procédure de mise en place de l'environnement d'exécution.

## Comptes
Prévoir un compte de service non-privilégié mais éligible à l'accès RDP pour pouvoir exécuter l'agent.

1. Créer le compte tf-agent en tant que compte local sans compte microsoft. Le compte doit être de type "Utilisateur standard"
2. Donner à ce compte le droit "accès au bureau distant"
3. Se connecter en tant que tf-agent pour initialiser ses répertoires utilisateur. Lors de la phase d'initialisation, refuser les télémesures, publicités ciblées et Cortana.
4. Se déconnecter de tf-agent pour se reconnecter sur un compte d'administrateur.

## Agent Squash
L'agent Squash est un script python qui demande une installation python 3.8 ou plus.

1. Télécharger l'installeur python 3.x courant pour windows depuis python.org : choisir l'installeur windows 64 bits complet

2. Installer python en lançant l'installeur en tant qu'administrateur
    * écran 1:
        * cocher les options "Installer python runner pour tous" et "Ajouter python 3.9 au PATH"
        * Cliquer sur Installation customisée
        ![Pyton Installer Pg1](img/pythonInstallerPg1.png "Python installe page 1")
    * écran 2:
        * décocher les cases Documentation, tcl/tk and IDLE, Python test suite
        * pip doit être sélectionné
        * for all users doit être sélectionné
        * cliquer sur "Next"
        ![Pyton Installer Pg2](img/pythonInstallerPg2.png "Python installe page 2")
    * écran 3:
        * cocher "Install for all users"
        * cliquer sur "Install"
        ![Pyton Installer Pg3](img/pythonInstallerPg3.png "Python installe page 3")

3. Installer les dépendances
    * Ouvrir une console de commandes en tant qu'administrateur
    * Exécuter la commande suivante:
```
python -m pip install requests
```

4. Créer un répertoire tf-agent dans le répertoire Documents du compte de service tf-agent
    * Ce répertoire doit être accessible en lecture seule + execution normalement pour l'utilisateur tf-agent :
        * Ouvrir les droits avancés sur le répertoire 
        * Désactiver l'héritage des droits sur tf-agent en choisissant de convertir les droits hérités en droits explicites
        * Sélectionner l'utilisateur tf-agent dans la liste des entrées d'autorisations et cliquer sur "Modifier"
        * Cliquer sur Afficher les autorisations avancées
        ![droits tf-agent](img/tfagentModifyRights.png "Modifer dcroits agent")
        * cliquer sur OK, puis sur Appliquer et OK
    * Copier dans le répertoire tf-agent le fichier agent.py et le fichier startup.cmd. Leur laisser les droits hérités du répertoire tf-agent.
    * Dans le fichier startup.cmd, remplacer le place holder <hush> par le vrai token dans la ligne :
```
    set TOKEN=\<hush\>
```
    * Créer les répertoires `tf-agent\scripts` et `tf-agent\workspace` où seront créés les workspaces utilisés lors des exécutions. Ces deux dossiers doivent être accessibles en lecture et écriture à l'utilisateur tf-agent (avec droits d'exécution) : désactiver l'héritage et ajouter les droits de suppression de fichiers et sous-dossiers.
    ![Droits pour les dossiers scripts et workspaces](img/droitsDossiersScriptsWorkspace.png "Droits scripts/workspaces")

## Git

Git est nécessaire pour toute exécution de test, puisque c'est l'outil utilisé pour déployer le dépôt de tests.

1. Télécharger l'installeur git pour windows depuis https://git-scm.com/download/win
(version de référence pour ce document : Git-2.26.0-64bits.exe)

1. Exécuter le fichier Git-2.26.0-64bits.exe en tant qu'administrateur

   ![Executer en tant qu'administrateur](img/git_execute_setup_as_admin.png "Git Setup as admin")

1. Passer l'écran de licence en cliquant sur "Next"

   ![Executer en tant qu'administrateur](img/git_install_1.png "Git install 1")
   
1. Accepter l'emplacement d'installation par défaut en cliquant sur "Next"

   ![Executer en tant qu'administrateur](img/git_install_2.png "Git install 2")

1. Choisir les options d'installation minimale pour le ligne de commande.

   ![Executer en tant qu'administrateur](img/git_install_3.png "Git install 3")

1. Accepter les options par défaut en cliquant sur "Next" pour le menu et l'éditeur par défaut

   ![Executer en tant qu'administrateur](img/git_install_4.png "Git install 4")
   ![Executer en tant qu'administrateur](img/git_install_5.png "Git install 5")

1. Choisir l'option d'intégration "de Git seul dans le PATH", cliquer sur "Next"

   ![Executer en tant qu'administrateur](img/git_install_6.png "Git install 6")
   
1. Conserver les options par défaut sur les écrans suivants en cliquant directement sur "Next"

   ![Executer en tant qu'administrateur](img/git_install_7.png "Git install 7")
   ![Executer en tant qu'administrateur](img/git_install_8.png "Git install 8")
   ![Executer en tant qu'administrateur](img/git_install_9.png "Git install 9")

1. Installer en cliquant sur "Install"

   ![Executer en tant qu'administrateur](img/git_install_10.png "Git install 10")
   
  


