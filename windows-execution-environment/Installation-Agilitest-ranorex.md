# Windows test execution environment (Agilitest/Ranorex)

Cette machine virtuelle est dédiée à l'exécution des tests sous windows sous pilotage de l'orchestrator.

En date du 24/03/2021 elle est réservée à l'orchestrateur Squash de l'instance "graphic-windows", en attente d'un mécanisme permettant
de partager la machine entre plusieurs orchestrateur **à tour de rôle** (afin d'éviter les conflits au niveau de la session graphique).

## Installation de l'hôte
L'hôte est une VM windows, avec les besoins suivants : être accessible par RDP depuis l'extérieur (permet d'activer la couche graphique)

Spécifications :

    Processeur : 2 cœurs (dixit agilitest)
    Mémoire RAM : 4 Go.
    Disque : ce qu'il faut à windows + 20 Go de marge pour installer les composants et pouvoir travailler
    OS : windows 10
    Firefox installé

De plus, elle doit pouvoir accéder aux instances du kube en HTTPS.

## Installation logicielle
Effectuer cet installation en étant connecté en tant qu'administrateur

### Mise en place de l'agent

Exécuter la procédure commune de mise en place de l'agent détaillée [ici](InstallationBaseEnvironnement.md)
Dans le fichier `startup.cmd`, modifier la définition de la variable `TAGS` comme suit :

	set TAGS=windows,agilitest,ranorex
### Navigateurs

Exécuter la procédure commune de mise en place des navigateurs détaillée [ici](InstallationNavigateurs.md)

### Outil agilitest

1. Télécharger les installeur pré-requis
    * JDK
    Choisir la dernière version du JDK11 depuis https://adoptopenjdk.net
        * Operating system : windows
        * Architecture: x64
        * Télécharger l'installeur .msi pour le JDK
    sha512 : 1c095ed556eda06c6d82fdf52200bc4f3437a1bab42387e801d6f4c56e833fb82b16e8bf0aab95c9708de7bfb55ec27f653a7cf0f491acebc541af234eded94d
    * Outils agilitest
    Télécharger les composants ATS depuis http://www.actiontestscript.com/tools/ats.zip

2. Installer le JDK
    * Exécuter l'installeur msi du JDK depuis le compte administrateur.
        * Lors de la sélection des options, demander à ce que les outils soient placés dans le PATH, associé aux jars et à ce que le JDK soit référencé par la variable JAVA_HOME
    ![Options installeur](img/parametresMSIJDK11.png "Otions d'installation JDK")
        * Cliquer sur "Suivant"
        * Confirmer en cliquant sur "Installer" (déclenche l'UAT)

3. Installer ActionTestScript
    * Copier l'archive `ats.zip` dans son répertoire de travail
    * Y dézipper l'archive
    * Créer `C:\Program Files\ats`
    * Y copier le répertoire ats-x.y.z extrait de l'archive
    ![Emplacement ATS](img/copyAtsInProgram.png "Emplacement ATS")
    * Ajouter aux variables d'environnement **globales** `ATS_HOME=C:\Program Files\ats\ats-x.y.z`
    ![Mettre ATS dans ATS_HOME](img/setGlobalAtsHome.png "Set global ATS_HOME")
4. Mettre en place les droits réseau pour le JDK
    * Basculer sur le compte de service
    * Démarrer l'agent
    * lancer un PEAC agilitest
    * Lors de l'apparition de la pop-up système, autoriser java à accéder aux réseaux privés et publics.
        ![Donner les autorisations réseau java](img/acceptJavaNetworkFirstAgilitest.png "Donner les autorisations réseau java")

### Maven
L'installation de cet outil peut servir à 
tester l'exécution sur windows de tests junit ou cucumber
1. Télécharger depuis http://maven.apache.org/ la version -bin.zip de maven 3.6.3
    lien : https://apache.mediamirrors.org/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip
    
2. Installer maven
    * Copier l'archive `apache-maven-3.6.3-bin.zip` dans son répertoire de travail
    * Dézipper l'archive dans son répertoire de travail
    * Créer le répertoire `C:\Program Files\Apache` (demande des privilèges)
    * Y copier la répertoire `apache-maven-3.6.2` extrait de l'archive (demande des privilèges)
    ![Emplacement maven](img/copyMavenInProgrammes.png "Emplacement maven")
    * Ajouter au PATH **global** le répertoire `C:\Program Files\Apache\apache-maven-3.6.3\bin`
    ![Add maven to path 1](img/addMavenToPath_1.png "Add maven to global path 1")
    ![Add maven to path 2](img/addMavenToPath_2.png "Add maven to global path 2")

3. Configurer maven pour l'agent :
    * Copier le fichier [settings.xml](settings.xml) ci-joint dans le répertoire `.m2` de l'utilisateur de l'agent.