set HOST=https://demo.new-forge.squashtest.org
set PORT=443
set PATH_PREFIX=graphic-windows-tf2-tm-enterprise/agentchannel
set TOKEN=<hush>
set TAGS=windows,agilitest
set SCRIPT_PATH=%~dp0scripts
set WS_DIRECTORY=%~dp0workspace
@REM For now we need utf-8

chcp 65001

python %~dp0\agent.py --host %HOST% --port %PORT% --path_prefix %PATH_PREFIX% --token %TOKEN% --tags %TAGS% --script_path=%SCRIPT_PATH% --workspace_dir=%WS_DIRECTORY%
