### Deploiement de cypress-agent pour TF
Ceci est un agent dédié à exécuter des tests cypress sur SquashTM2.
Un pod contient 3 conteneurs (stateless):

1. cypress
2. TM
3. postgres

Pour se connecter en SSH sur l'agent depuis le cluster:

	`ssh squashtf@cypress-agent.agents.svc.cluster.local`
	
! L'authentification n'est possible que via une clé publique. 

#### Prérequis pour le déploiement:
Credentials:

1. Secret 'ssh-key' 

	Ce secret doit contenir le fichier de la clé publique dédiée à la connexion SSH avec l'ordonnanceur. 
	Cette clé est ensuite copiée dans .ssh/authorized_keys de l'agent.
	Le secret peut être créé à l'aide de la commande : 
		`kubectl -n agents create secret generic ssh-key --from-file=id_rsa.pub=/path/to/.ssh/id_rsa.pub`


2. Secret 'jenkins-bitbucket-credentials-ssh'

	Ce secret doit fournir une clé privée SSH pour se connecter aux repos de bitbucket.org. Cette clé va être référencée  au .ssh/config de l'agent. 
	Pour créer le secret: 
		`kubectl apply -f bitbucket-credentials-ssh.yaml`
	Actuellement le compte de jenkins est utilisé.

#### Déploiement

La commande suivante vas ajouter le deployment de l'agent 'cypress' ainsi qu'un service 'cypress-agent'

`kubectl -n agents apply -f deployment-cypress-agent.yaml`