# Configuration des volumes

Par dessus le montage k3s nous avons besoin de finaliser l'installation de nos volumes. Nous utiliserons le driver 
OpenEBS (https://openebs.io/) car il nous offre plusieurs StorageClass alternatives par rapport à celle proposée par défaut.
En particulier il propose la classe `openebs-hostpath`, qui comme son nom ne l'indique pas est en réalité driver pour volume 
local (ce qui est différent).


## Installation

L'installation d'OpenEBS se fait simplement par un chart Helm (pour minikube voir plus bas) :

    kubectl create ns openebs
    helm repo add openebs https://openebs.github.io/charts
    helm repo update
    helm install openebs openebs/openebs -n openebs \
            --set ndm.tolerations[0].key="node-role.henix.com/dedicated-node" \
            --set ndm.tolerations[0].operator="Exists" \
            --set ndm.tolerations[0].effect="NoSchedule" 
    
TODO: On note l'ajout d'une toleration pour le daemonset `openebs-ndm` à propos des dedicated-nodes. 
C'est un peu verbeux, si nécessaire on pourra porter tout ça dans un fichier de values.   
    
On peut vérifier que les nouvelles StorageClass sont disponibles :

    kubectl get sc
    
Note : si on souhaite altérer certaines helm values, la liste est disponible sur le projet github [1].
 
 
    
## Focus : Minikube

Du fait que Minikube wipe le disque à chaque démarrage à l'exception de certains dossiers [2], le driver OpenEBS a besoin
d'un peu plus de configuration si on souhaite persister les données au-delà du redémarrage. Il faut pour cela changer 
le répertoire pour un endroit plus sûr, par exemple `/var/lib/kubelet` :

    helm install -n openebs openebs openebs/openebs \
        --set varDirectoryPath.baseDir="/var/lib/kubelet/openebs" \
        --set localprovisioner.basePath="/var/lib/kubelet/openebs/local" \
        --set ndm.sparse.path="/var/lib/kubelet/openebs/sparse"
        --set jiva.defaultStoragePath="/var/lib/kubelet/openebs"


On peut vérifier que les StorageClass pointent vers le nouveau répertoire en inspectant par exemple la définition de 
`openebs-hostpath` :

    kubectl describe sc openebs-hostpath
    
Note : désinstaller proprement openebs avant d'effectuer la manip car un simple `helm uninstall` ne supprimera pas pour 
autant la définition des StorageClass, qui ne seront donc ni recréées ni altérées par votre nouvelle installation.

### Optionnel : configurer la StorageClass par défaut

On peut aussi faire de la StorageClass `openebs-hostpath` la classe par défaut. Il faut pour cela 
retirer le flag 'default' à l'actuel default et l'accoler à la classe susnommée. Cependant nos best-practices recommandent 
de préciser explicitement la storage classe dans la définition de nos Deployments.

    kubectl annotate sc storageclass.kubernetes.io/is-default-class-  --all
    kubectl annotate sc openebs-hostpath storageclass.kubernetes.io/is-default-class=true


## Désinstaller OpenEBS

Avant de désinstaller OpenEBS, vérifier qu'aucun volume persistent n'utilise de volume OpenEBS :

    kubectl get pv | grep openebs
    
Une fois que c'est vérifié, supprimer avec :

    helm -n openebs uninstall openebs
    for a in $(kubectl get sc -o name | grep openebs); do kubectl delete  $a; done



## Sources :
[1] https://github.com/openebs/charts/blob/master/charts/openebs/values.yaml
[2] https://minikube.sigs.k8s.io/docs/handbook/persistent_volumes/



----

# Annexe 

Les éléments ici apportent des précisions sur le choix de la solution OpenEBS.

## Pourquoi changer les drivers des PersistentVolumes

La raison de changer la gestion des volumes et directement liée à notre besoin de backup. En effet, Velero a besoin d'une 
de ces solutions au choix (plus d'informations dans le document `cluster-global/velero/etude-velero.md`) :

1. Solution générique Kubernetes (1.18+) : que les volumes soient des PersistentVolumes, pilotés par des drivers CSI, qui supportent la fonctionnalité de VolumeSnapshot, 
2. Solution addhoc : que les volumes soient des Volumes simples, mais pour lesquels il existe un driver *in tree* qui supporte leur propre fonction de 
snapshot (par exemple un volume  `AWS`), et qu'il existe un plugin côté Velero pour ce provider.
3. Solution Restic : les volumes sont des PersistentVolumes par forcément CSI mais au moins ne sont pas du type HostPath.

Les drivers natifs fournis par K3S ou Minikube ne rentrent dans aucune de ces catégories : ce sont des drivers non-CSI, de type 
HostPath, et pour lesquels aucun plugin Velero n'existe.
 
Pour solutionner le problème nous optons ici pour un driver permettant d'utiliser la solution 3, ici par le biais des drivers 
OpenEBS.

A long terme le choix d'un driver CSI aurait été judicieux : c'est dans cette direction que souhaite aller la core team de 
Kubernetes [1]. Cependant il est compliqué de trouver un driver CSI qui soit stable et simple (voir ci-dessous). 

Par ailleurs les StorageClass OpenEBS, en particulier `openebs-hostpath`, plus spécifiquement le provisionner (à ne pas confondre
avec un 'driver') qui s'appuient sur la feature de volumes locaux [2], vont intégrer à court terme la distribution K3S pour 
devenir le nouveau standard (si ce n'est pas déjà fait) [3], [4]. Enfin, OpenEBS fourni un plugin dédié pour Velero [5], 
ce qui permettrait potentiellement de passer à une solution de type 2 (cad, snapshots sans Restic).


## Alternatives : Ceph ou Longhorn

Le driver CSI le plus simple répondant à notre besoin est le driver csi-hotspath [6].
Le CSI hostpath driver, comme son nom l'indique, est un driver qui implémente CSI, et qui s'appuie physiquement sur le filesystem 
local du node, malheureusement en tant qu'exemple d'implémentation il souffre de défauts qui ne le rendent pas production-grade, 
que nous avons constatés y compris sur nos environnements [7].

On préfèrera dans ce cas des solutions plus sophistiquées telles que Rook+Ceph [8] ou Longhorn [9]. Cependant ces solutions sont 
justement dimensionnées pour ces échelles gigantesques et à ce titre réclament des fermes dédiées uniquement au stockage 
(des nodes avec juste des disques, ou des volumes AWS ou GKE par exemple). Nos opérations n'ont clairement pas cette ampleur, 
aussi pour restreindre les coûts et maintenir des temps d'accès optimaux nous continuons de privilégier un stockage localisé 
physiquement sur le node.
 
## Sources 

[1] https://kubernetes.io/blog/2019/12/09/kubernetes-1-17-feature-csi-migration-beta/
[2] https://kubernetes.io/blog/2019/04/04/kubernetes-1.14-local-persistent-volumes-ga/
[3] https://www.n0secure.org/2020/07/k3s-velero-a-long-way-to-devsecops-episode-6.html
[4] https://github.com/rancher/local-path-provisioner/pull/91
[5] https://github.com/openebs/velero-plugin
[6] https://github.com/kubernetes-csi/csi-driver-host-path
[7] https://github.com/kubernetes-csi/csi-driver-host-path/issues/55
[8] https://rook.io/
[9] https://rancher.com/products/longhorn/


