# Gestion des secrets


## Objectif :

Pouvoir stocker de façon sécurisée nos secrets, en particulier ne pas les faire apparaître dans les backups.
Deux types de secrets :
Les secrets nécessaires à l'accès au cluster Kube :
   - Token d'accès en tant qu'administrateur au cluster
   - Les unseal keys de Vault
     
Les secrets applicatifs :
   - Clé SSH Bitbucket
   - Identifiants de Jenkins sur Nexus
   - Identifiants pour la Docker registry
   - etc.


La solution consiste à utiliser un Keepass dans un Google Drive pour les secrets dit d'administration.

Pour les secrets applicatifs, ceux-ci seront stockés dans le Vault dans un secret engine de type kv (key=value)

Attention : le schéma ci-dessous montre la structure globale mais n'est pas indicative des secrets (et de leur path) 
dans Vault.

![Schema d'organisation des secrets](_static/arch_secret.jpg)

## Procédure d'ajout de secret dans Vault

Cette section concerne uniquement l'ajout de secret dans Vault et ne mentionne pas leur intégration à Kubernetes.

1 : Se connecter au container de Vault via Kubernetes.

2 : Se connecter à Vault en utilisant un compte utilisateur.

3 : Ajouter le secret voulu avec la commande 
```
vault kv put ${SECRET_PATH} ${KEY1}=${VALUE1} ${KEY2}=${VALUE2} ...
```
Il est possible de mettre un "-" à la place de la valeur pour ajouter une donnée multiligne comme une clé privée.

Notre secret engine kv est monté sur secretkv.

4 : Vérifier et modifier si nécessaire les policies de l'application qui sera concerné. Pour mettre à jour une policy, il n'existe pas de commande de mise à jour. 
Il faut récupérer celle existante (soit via le fichier de définition, soit avec la commande `vault policy read ${POLICY_NAME}`) et ajouter les modifications nécessaires.

Exemple d'écriture de policy :

```
vault policy write ${POLICY_NAME} - <<EOF
path "${SECRET_PATH}" {
    capabilities = ["read", "list"]
}
EOF
```

5 : Se déconnecter en supprimant le fichier ~/.vault-token

## Procédure d'intégration des secrets à Kubernetes (Vault et ExternalSecrets) :

L'outil ExternalSecrets permet de synchroniser des secrets Vault dans Kubernetes (en passant par l'engine spécifique à Kubernetes).

Ceci est la méthode par défaut de consommation des secrets dans notre cluster.

Cette procédure part du principe que le secret à intégrer a déjà été ajouté dans Vault.

1 : Créer un .yaml de type ExternalSecret avec globalement la syntaxe suivante puis l'appliquer. 
Attention à bien mettre le label `exclude-from-backup` pour que le secret généré ne soit pas ajouté au backup Velero.

```
apiVersion: 'kubernetes-client.io/v1'
kind: ExternalSecret
metadata:
  name: ${SECRET_NAME}
  namespace: ${NAMESPACE}
spec:
  backendType: vault
  vaultMountPoint: kubernetes
  vaultRole: ${VAULT_ROLE}
  data:
  - name: ${KUBERNETES_SECRET_KEY_NAME}
    key: ${VAULT_SECRET_ENGINE_PATH}
    property: ${VAULT_SECRET_PROPERTY_NAME}
  ...
  template:
    metadata:
      labels:
        ${LABEL_NAME}: ${LABEL_VALUE}
        "velero.io/exclude-from-backup": "true"
```

Exemple complet pour l'ajout d'une clé SSH accessible dans Jenkins: 

```
apiVersion: 'kubernetes-client.io/v1'
kind: ExternalSecret
metadata:
  name: extsecretgit
  namespace: jenkins
spec:
  backendType: vault
  vaultMountPoint: kubernetes
  vaultRole: extsecret
  data:
  - name: username
    key: secretkv/data/jenkins/git
    property: username
  - name: privateKey
    key: secretkv/data/jenkins/git
    property: ssh
  template:
    metadata:
      labels:
        "jenkins.io/credentials-type": "basicSSHUserPrivateKey"
        "velero.io/exclude-from-backup": "true"
```

Si le secret à générer n'est pas de type Opaque (par exemple s'il est de type dockerconfigjson), 
il est possible de lui ajouter un type dans la section template.

Exemple : 

```
apiVersion: 'kubernetes-client.io/v1'
kind: ExternalSecret
metadata:
  name: dockerconfigsecret
  namespace: jenkins
spec:
  backendType: vault
  vaultMountPoint: kubernetes
  vaultRole: extsecret
  data:
  - name: .dockerconfigjson
    key: secretkv/data/docker
    property: .dockerconfigjson
  template:
    type: kubernetes.io/dockerconfigjson
    metadata:
      labels:
        "velero.io/exclude-from-backup": "true"
```

2 : Dans les Custom Resource Definition du namespace concerné, un nouvel objet doit être présent dans les ExternalSecrets.

Le nouveau secret doit également être disponible.

Si le secret n'apparaît pas, il est possible de voir le statut de la synchronisation en consultant le CRD créé.


## Procédure d'intégration des secrets à Kubernetes (Vault et les injectors) :

L'injector permet de mettre à disposition des containers les secrets voulus via un volume partagé.

Cette injection se gère via des annotations, que ce soit sur des déploiements ou des podTemplates.

Un agent injector se lance dans le pod et va s'authentifier à Vault pour les récupérer.

Cette procédure part du principe que le secret à intégrer a déjà été ajouté dans Vault.

1 : Ajouter au podTemplate ou au déploiement les annotations nécessaires comme spécifié dans l'exemple suivant.

Détail des annotations :

  * agent-inject indique qu'un sidecar doit être instancié et que des secrets doivent être injecté.

  * role correspond au rôle d'authentification dans Vault. Par exemple, le rôle jenkins.

  * ca-cert correspond au .crt du SA. Sans cette annotation, l'agent n'arrivera pas à communiquer avec Vault (s'il est en https).
  
  * agent-inject-secret-${FILE_NAME} correspond au chemin du secret dans Vault. Ce qui se trouve après `secret-` sera le nom du fichier dans lequel seront renseignés les secrets.
  
  * agent-inject-template-${FILE_NAME} permet de formatter les secrets dans le fichier.

```
template:
  metadata:
    annotations:
      vault.hashicorp.com/agent-inject: "true"
      vault.hashicorp.com/role: "defaultuser"
      vault.hashicorp.com/ca-cert: "/run/secrets/kubernetes.io/serviceaccount/ca.crt"
      vault.hashicorp.com/agent-inject-secret-nexus-creds.txt: "secretkv/data/jenkins/nexus"
      vault.hashicorp.com/agent-inject-template-nexus-creds.txt: |
        {{- with secret "secretkv/data/jenkins/nexus" -}}
        {{  .Data.data.username }}:{{ .Data.data.password }}
        {{- end -}}
```

2 : Lancer le déploiement ou le pod. Les secrets sont ensuite accessibles dans /vault/secrets/{FILE_NAME}

## Procédure d'intégration des secrets à Kubernetes (Vault et son plugin sur Jenkins) :

En plus des autres méthodes pour utiliser les secrets dans Kubernetes, il est également possible de les utiliser via le plugin Vault sur Jenkins.

Ce plugin permet notamment deux cas d'utilisations : 

  * Récupération des secrets depuis le script d'une pipeline
  
  * Utilisation d'un secret en tant que Jenkins Credential

Cette procédure part du principe que le secret à intégrer a déjà été ajouté dans Vault et que le plugin est déjà configuré sur Jenkins.

### Récupération des secrets depuis une pipeline

Ci-dessous un exemple d'utilisation des secrets dans une pipeline.

A noter que les secrets n'apparaîtront pas dans la méthode withVault. 
Un `echo` du secret sera affiché sous forme d'étoiles dans les logs.

Les secrets sont ensuite accessible sous forme de variable d'environnement.

```
container('mvn') {
  def secrets = [
    [path: 'secretkv/jenkins/bitbucket', engineVersion: 2, secretValues: [
      [vaultKey: 'main']
    ]]]
  withVault([vaultSecrets: secrets]) {
    sh 'echo $main >> /opt/mysecretfile'
  }
    sh 'cat /opt/mysecretfile'
}
```
`
### Utilisation d'un secret en tant que Jenkins Credential

Avec le plugin Vault sur Jenkins, de nouvelles options sont disponibles dans le menu Credentials.

Ces options reprennent les formats standard (SSH Username with private key, etc.) en version Vault.

La configuration est semblable aux autres utilisations de Vault et nécessite donc le chemin du secret.

Selon le type de secret souhaité, il faut aussi relier la clé dans Vault avec le paramètre concerné.

Exemple :
```
Username key : username
Private Key Key : ssh_key
Passphrase Key : pass
``` 

Le secret est ensuite utilisable comme un credential classique.

## Procédure d'ajout de secrets dans la base Keepass :

1 : Récupérer la base Keepass depuis le Google Drive. 
Cette base est au format .kdbx et est disponible dans le drive des administrateurs du Kube.

2 : Aller demander aux OPS pour avoir accès à la master key puis ouvrir la base récupérée.

3 : Dans Keepass, créer le nouveau secret à l'emplacement souhaité.

4 : Une fois tous les secrets ajoutés ou modifiés, sauvegarder puis envoyer la nouvelle base sur le drive.

## Rotation des logs :

Les logs d'audit prenant une place non négligeable, une rotation des logs a été mise en place pour gérer
ceux-ci.

Ceci se fait via un side container (forge/vault-rotate) qui a les particularités suivantes :
 
  * Un volume partagé avec le container de Vault sur le dossier `/vault/audit`
  
  * Une base alpine avec ajout d'un utilisateur et d'un groupe vault pour pouvoir créer un nouveau fichier de log 
    accessible par le processus initial
    
  * Logrotate via crond pour gérer cette rotation. Un signal est envoyé à chaque fin de rotation pour que Vault puisse
    charger et écrire dans le nouveau fichier. Afin que ce signal puisse être envoyé depuis le side container, le
    partage des processus a été activé. 
    
La politique mise en place est la suivante :

  * Rotation des logs tous les jours.
  
  * 10 fichiers sont conservés
  
  * La compression ne se fait pas à la rotation mais à la prochaine pour ne pas perdre des entrées lors de la rotation

L'image du side container est disponible dans notre Nexus et le Dockerfile dans le dossier `resources`.

## Mise en place des outils :

La grande majorité des commandes de cette section utilisent des variables d'environnement.

### Configuration du TLS et installation de Vault

Le certificat utilisé pour le TLS est self-signed.

La procédure suivante part du principe qu'on doit générer une nouvelle clé.

Les commandes peuvent être réalisé depuis n'importe quel poste qui dispose de kubectl.

1 : Générer une clé avec la commande suivante : `openssl genrsa -out ${VAULT_KEY} 2048`

2 : Créer ensuite un fichier de configuration pour le CSR (Certificate Signing Request).

```
cat <<EOF >${VAULT_CSR_CONF}
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
[req_distinguished_name]
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names
[alt_names]
DNS.1 = ${SERVICE_NAME}
DNS.2 = ${SERVICE_NAME}.${NAMESPACE}
DNS.3 = ${SERVICE_NAME}.${NAMESPACE}.svc
DNS.4 = ${SERVICE_NAME}.${NAMESPACE}.svc.cluster.local
IP.1 = 127.0.0.1
EOF
```

3 : Générer ensuite un .csr avec la clé et le fichier de configuration grâce à la commande suivante :

`openssl req -new -key ${VAULT_KEY} -subj "/CN=${SERVICE}.${NAMESPACE}.svc" -out ${VAULT_CSR_FILE} -config ${VAULT_CSR_CONF}`

4 : Utiliser le .csr généré pour obtenir un yaml qui pourra être utilisé avec Kubernetes.

```
cat <<EOF >${CSR_YAML}
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: ${CSR_NAME}
spec:
  groups:
  - system:authenticated
  request: $(cat ${VAULT_CSR_FILE} | base64 | tr -d '\n')
  usages:
  - digital signature
  - key encipherment
  - server auth
EOF
```

5 : Envoyer ensuite le CertificateSigningRequest et l'approuver.

```
kubectl create -f ${CSR_YAML}
kubectl certificate approve ${CSR_NAME}
```

6 : Générer un .crt avec la commande suivante : 

`echo "$(kubectl get csr ${CSR_NAME} -o jsonpath='{.status.certificate}')" | openssl base64 -d -A -out ${VAULT_CRT}`

7 : Générer un .ca avec la commande suivante :

`kubectl config view --raw --minify --flatten -o jsonpath='{.clusters[].cluster.certificate-authority-data}' | base64 -d > ${VAULT_CA}`

8 : Créer le namespace où sera installé Vault s'il n'existe pas : `kubectl create ns ${NAMESPACE}`

8 : Créer un secret contenant l'ensemble des informations.

```
kubectl create secret generic ${VAULT_TLS_SECRET_NAME} \
        --namespace ${NAMESPACE} \
        --from-file=${VAULT_KEY} \
        --from-file=${VAULT_CRT} \
        --from-file=${VAULT_CA}
```

9: Substituer les valeurs du fichier de valeur de la Helm chart Vault.
Attention : Le side container utilisé pour la rotation des logs se trouve dans notre Nexus dont l'accès est restreint.
Il faut ajouter aux values de la charte l'attribut `imagePullSecrets` avec le secret correspondant dans la section `global`.


`envsubst < custom_vault.yaml > custom_vault_sub.yaml`

10 : Installer Vault avec la configuration dans custom_vault.yaml.
Ne pas oublier de modifier les valeurs du .yaml pour correspondre avec ce qui est présent dans le cluster.

```
helm repo add hashicorp https://helm.releases.hashicorp.com
helm install vault hashicorp/vault --values custom_vault_sub.yaml -n ${NAMESPACE}
```

### Configuration de Vault

Dans cette section, les variables entre accolades sont à remplacer par les valeurs souhaitées.

Celles précédées juste par un $ (exemple : $KUBERNETES_PORT_443_TCP_ADDR:443) sont des variables d'environnement et peuvent donc être utilisées tel quel. 

#### Initialisation et unseal du Vault

Cette section concerne la configuration du Vault une fois l'installation terminée.

Les instructions suivantes partent du principe que l'on est connecté au container de Vault.

Pour initialiser un nouveau Vault, utiliser la commande suivante :

`vault operator init`

On obtient ensuite 5 unseal keys et un root token.

Ces unseal keys sont à stocker dans le Keepass. Le root token doit être utilisé dans un premier temps pour faire la première configuration.

Suite à l'initialisation, il faut tout d'abord déverouiller le Vault avec les unseal keys obtenus (`vault operator unseal`).

#### Connexion à Vault

Avant d'interagir avec Vault, il faut tout d'abord se connecter.

Suite à l'installation, seul le root token existe. On doit donc l'utiliser grâce à la commande :

`vault login`

Le token sera ensuite demandé en prompt.

#### Activation du secret engine kv2

Le secret engine kv2 (key=value 2) est le principal utilisé. Celui-ci s'active avec la commande suivante :

`vault secrets enable -version=2 -path=${SECRET_ENGINE_PATH} kv`

Notre chemin par défaut est `secretkv`.

#### Création d'utilisateurs (administrateur)

La première étape à faire est d'activer l'authentification userpass : `vault auth enable userpass`

Il faut ensuite créer une policy pour les administrateurs. Utiliser la commande suivante tout en modifiant les différents path pour les secret engines.

```
vault policy write admin - <<EOF
# Read system health check
path "sys/health"
{
  capabilities = ["read", "sudo"]
}

# Create and manage ACL policies broadly across Vault

# List existing policies
path "sys/policies/acl"
{
  capabilities = ["list"]
}

# Create and manage ACL policies
path "sys/policies/acl/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Enable and manage authentication methods broadly across Vault

# Manage auth methods broadly across Vault
path "auth/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Create, update, and delete auth methods
path "sys/auth/*"
{
  capabilities = ["create", "update", "delete", "sudo"]
}

# List auth methods
path "sys/auth"
{
  capabilities = ["read"]
}

# Enable and manage the key/value secrets engine at `secret/` path

# List, create, update, and delete key/value secrets
path "secretkv/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Manage secrets engines
path "sys/mounts/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# List existing secrets engines.
path "sys/mounts"
{
  capabilities = ["read"]
}
EOF
```

Créer ensuite un ou des administrateurs avec la commande suivante :

```
vault write auth/userpass/users/${USERNAME} \
    password=${PASSWORD} \
    policies=admin
```

Il sera ensuite possible de se connecter avec `vault login -method=userpass username=${USERNAME}`

Une fois l'administrateur créé, on peut supprimer le root token avec la commande `vault token revoke -self`

Autrement, on peut se déconnecter en supprimant le fichier ~/.vault-token

#### Gestion des logs

Les logs peuvent s'activer via la commande suivante :

`vault audit enable file file_path=/vault/audit/audit.log`

L'activation des logs doit se faire avec le root token car la policy admin ne gère pas le module audit.

#### Configuration de Vault pour une utilisation avec Kubernetes

1 : Activation de la méthode d'authentification via les Service Accounts Kubernetes

`vault auth enable kubernetes` 
 
2 : Configuration de la méthode d'authentification

```
vault write auth/kubernetes/config \
    token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
    kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443" \
    kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
```

3 : Création d'une policy et configuration du rôle associé. Ceci consiste à créer un rôle pour un ServiceAccount Kubernetes et de gérer ses droits d'accès.

Attention : le ${SECRET_PATH} doit être sous la forme suivante : `${SECRET_ENGINE_MOUNT}/data/${SECRET_PATH}`

On note ici la séparation de l'engine avec le chemin du secret par un `data`. 

```
vault policy write ${POLICY_NAME} - <<EOF
path "${SECRET_PATH}" {
    capabilities = ["read", "list"]
}
EOF
```

```
vault write auth/kubernetes/role/${ROLE_NAME} \
    bound_service_account_names=${SERVICE_ACCOUNT_NAME} \
    bound_service_account_namespaces=${SERVICE_ACCOUNT_NAMESPACE} \
    policies=${POLICY_NAME} \
    ttl=24h
```

### Configuration d'External Secrets

1 : Substituer les valeurs dans le fichier de valeur d'External Secrets avec les bonnes variables d'environnement :

`envsubst < custom_ext_secret.yaml > custom_ext_secret_sub.yaml`

2 : Installer External Secrets dans le cluster via la Helm Chart avec la configuration présente dans custom_ext_secrets.yaml.
Attention à bien modifier les valeurs du .yaml pour que le fichier soit cohérent avec ce qui est présent dans Kubernetes.

```
helm repo add external-secrets https://external-secrets.github.io/kubernetes-external-secrets/
helm install extsecrets external-secrets/kubernetes-external-secrets -f custom_ext_secret_sub.yaml -n ${NAMESPACE}
```

3 : Créer ensuite dans Vault un rôle avec le service account d'externalsecrets et la policy qui lui correspond. 

```
vault policy write extsecret - <<EOF
path "${SECRET_PATH}" {
    capabilities = ["read", "list"]
}
EOF
```

```
vault write auth/kubernetes/role/extsecret \
    bound_service_account_names=extsecrets-kubernetes-external-secrets \
    bound_service_account_namespaces=${NAMESPACE} \
    policies=extsecret \
    ttl=24h
```

### Configuration du plugin Vault dans Jenkins

Pour configurer le plugin, il faut juste configurer deux champs :
  * L'URL au format suivant : `https://${SERVICE_NAME}.${NAMESPACE}.svc:${PORT_NUMBER}`
  * L'ajout d'un credential Jenkins de type Vault Kubernetes Credential. 
    Le `Mount Path` correspond au chemin de la méthode d'authentification sur Vault. vault
    Le `role` correspond au rôle associé au Service Account de Jenkins dans Vault.
 
Pour gérer le TLS depuis Jenkins, deux ajouts sont nécessaires.
Dans un premier temps, il faut réaliser une première opération manuelle qui consiste à modifier le keystore Java et de lui injecter le .crt du service account.
Ceci se fait avec la commande suivante :

`keytool -import -alias sacert -keystore /usr/local/openjdk-8/jre/lib/security/cacerts -file /var/run/secrets/kubernetes.io/serviceaccount/ca.crt` 

Ce keystore doit ensuite être dupliqué dans le volume de Jenkins pour qu'il soit conservé en cas de redéploiement. 

Un exemple de chemin est `/var/jenkins_home/keystore/cacerts`.

La seconde étape consiste à ajouter dans les JAVA_OPTS l'emplacement du nouveau keystore.

`-Djavax.net.ssl.trustStore=/var/jenkins_home/keystore/cacerts`

Cet ajout peut se faire via la Helm chart de Jenkins en ajoutant dans les valeurs la ligne suivante :

`javaOpts: -Djavax.net.ssl.trustStore=/var/jenkins_home/keystore/cacerts`

## Générer un nouveau root token

Si le root token est révoqué, il est possible de le générer à nouveau.

L'accès aux unseal keys est nécessaire pour cette génération.

Sur le container de Vault, exécuter la commande suivante :

`vault operator generate-root -init`

Il faut ensuite garder l'OTP généré car il sera utilisé pour décoder le token.

Exécuter ensuite 3 fois la commande `vault operator generate-root` et insérer à chaque fois une unseal key. 

On obtient ensuite un "Encoded Token" qu'il faut décoder avec l'OTP.

Exécuter la commande suivante :

```
vault operator generate-root \
  -decode=${ENCODED_TOKEN} \
  -otp=${OTP}
```

On obtient en sortie le nouveau token.

## Informations complémentaires :

Des Google Slides sont disponibles pour plus d'informations (complémenté d'images).

Vault : https://docs.google.com/presentation/d/1fzvepL8bWdoAjzKiu0ogpvIUq5lrEPQbLeM2VP5IRkk/edit?usp=sharing

Keepass : https://docs.google.com/presentation/d/193DsrxvWpESZUqTR6O7pViprQJLpblVUPHTKAWPgHIo/edit?usp=sharing



