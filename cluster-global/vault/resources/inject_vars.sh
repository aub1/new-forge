#!/bin/bash
export VAULT_TLS_SECRET_NAME=vault-tls
export SERVICE_NAME=vault
export NAMESPACE=vault
export CSR_NAME=vault.csr
export VAULT_CSR_CONF=vault.csr.conf
export VAULT_CSR_FILE=vault.csr
export VAULT_KEY=vault.key
export CSR_YAML=csr.yaml
export VAULT_CRT=vault.crt
export VAULT_CA=vault.ca
