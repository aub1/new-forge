# Mode opératoire de Velero

Notre solution de backup est bâtie sur Velero + Restic, mais pour que le contenu des volumes soit aussi embarqué dans un 
backup il est nécessaire d'installer OpenEBS au préalable (voir `cluster-global/storage/config-volumes.md`). 
Nous choisissons ici la StorageClass `openebs-hostpath` car il est adapté à notre situation. 

Plus de details dans le document joint `cluster-global/velero/etude-velero.md`.


## Périmètre du backup

Que ça doit en terme de backup ponctuel ou planifié, nous souhaitons sauvegarder les éléments suivants: 

* Dans le namespace jenkins : 
    - le Pod de Jenkins, mais pas les autres (pods d'agents notamment)
    - aucun secret sauf les token pour les services account
    - tout le reste
    
* Dans le namespace vault :
    - tout


La politique est opt-out : les ressources qu'on ne veut pas voir dans le backup doivent recevoir le label `velero.io/exclude-from-backup=true`.
A l'usage c'et beaucoup moins risqué que opt-in, car mieux vaut voir une ressource inclue par erreur qu'oubliée par erreur.


## Installer OpenEBS

Cette étape est décrite dans `cluster-global/storage/config-volume.md`, et est normalement déjà effectuée à ce stade.


## Installation de Velero


Download de Velero et installation du binaire :

    wget https://github.com/vmware-tanzu/velero/releases/download/v1.5.1/velero-v1.5.1-linux-amd64.tar.gz
    tar -xvf velero-1.5.1-linux-amd64.tar.gz -C /opt

Ici la version retenue est 1.5.1 mais on n'est bien sûr pas verrouillé à celle-ci. Ensuite : 

    cd /opt/velero-v1.5.1-linux-amd64
    mv velero /usr/bin
    
Créer un fichier contenant les accès au     
    
    cat << 'EOF' > credentials-velero
    [default]
    aws_access_key_id = <id>
    aws_secret_access_key = <key>    
    EOF    

Ensuite, une fois que les credentials S3 sont configurés, exécuter : 

    velero install \
      --provider aws \
      --plugins velero/velero-plugin-for-aws:v1.0.0 \
      --bucket newforge-velero \
      --secret-file ./credentials-velero \
      --use-volume-snapshots=false \
      --use-restic \
      --default-volumes-to-restic \
      --backup-location-config s3Url=https://s3.fr-par.scw.cloud,region=fr-par


Note : avec ces options la sauvegarde des volumes est :
- assurée par Restic,
- automatique : ça veut dire que par défaut tous les volumes seront embarqués (sauf les configmap et secrets), à moins de 
les exclure. 
      
Il existe aussi un support de l'autocompletion bash (voir la commande `velero completion` pour plus de détails).      

## Donner les teintes requises à Restic

Se référer au besoin au fichier `cluster-global/k3s/taints.md`. Pour référence voici ce qu'on peut rajouter ici : 

    kubectl -n velero edit daemonset restic
    
Et rajouter : 
    
    spec:
      template:
        spec:
          tolerations:
          - key: "node-role.henix.com/dedicated-node"
            operator: "Exists"
      
## Planifier les backups

Si nécessaire éditer le fichier `velero-jenkins-schedule.yaml` (ou un autre le cas échéant), puis le créer avec `kubectl` :

    kubectl -n velero apply -f velero-cluster-schedule.yaml
    
La documentation pour cette ressource est ici :  https://velero.io/docs/v1.5/api-types/schedule/#docs
On peut vérifier l'avancement du premier backup (InProgress ou Completed) avec :
    
    velero backup get

Puis qu'on a bien tout le contenu souhaité dans le backup (en particulier les volumes et que les secrets importants sont exclus) :

    velero backup describe <nombackup> --details | less 


Note : à cause d'un bug encore ouvert (https://github.com/vmware-tanzu/velero/issues/2612), lorsqu'un objet Schedule 
est créé un backup et déclenché immédiatement. Cela signifie qu'il faut attendre le bon moment pour créer cet objet.

Note : la façon normale de planifier un backup, cad un objet Schedule, se fait normalement par la commande suivante (exemple) :

    # Exemple de commande : 
    velero schedule create jenkins-back --schedule="0 0/24 * * *" --include-namespaces="jenkins,vault"
    
Cependant puisque le périmètre d'un schedule est amené à varier, utiliser un descripteur yaml est plus intéressant autant 
pour faciliter la manipulation que pour son aspect documentaire.
    
    
## Exclure un volume d'un pod 

Par default, Restic (en mode `--default-volumes-to-restic`) va prendre tous les volumes d'un pod sauf les configmaps et 
les secrets. De cette façon on se prémunit contre les oublis malencontreux lorsque de futurs Deployments etc seront ajoutés.

Pour exclure explicitement un volume il faut lui rajouter une *annotation* :

    kubectl -n <namespace> annotate <type> <ressource> backup.velero.io/backup-volumes-excludes=volume1,volume2

En général on voudra exclure de cette façon les volumes de type EmptyDir etc.

Note : penser à appliquer cette *annotation* pas seulement sur le pod, mais surtout sur son Deployment/ReplicaSet/DaemonSet 
ou encore sur les Helm values. Ceci afin que si le pod est redéployé, cette annotation soit restaurée avec.
On peut parfois retrouver cette information en inspectant la ressource avec `kubectl describe` et en cherchant l'item `Controlled By`.

    
## Exclure une ressource du périmètre du backup

Si la ressource fait partie d'un namespace non couvert il ne sera pas inclus. Dans le cas contraire il faut l'exclure 
en rajoutant un *label* : 

    kubectl -n <namespace> label <type> <ressource> 'velero.io/exclude-from-backup=true'
    

Note : penser à appliquer ce *label* pas seulement sur le pod, mais surtout sur son Deployment/ReplicaSet/DaemonSet 
ou encore sur les Helm values. Ceci afin que si le pod est redéployé, ce label soit restaurée avec.
On peut parfois retrouver cette information en inspectant la ressource avec `kubectl describe` et en cherchant l'item `Controlled By`.
    
    
## Créer manuellement un backup

Dans le cas où il est nécessaire de créer un backup sur le champ pour une raison quelconque :

    velero backup create --from-schedule velero-jenkins-schedule
    
En général c'est ce qu'on veut. On peut aussi en créer sur mesure, se référer à la documentation de Velero pour cela.

## Restaurer un backup

La procédure de restauration consiste à dropper les namespaces concernés, puis à créer un objet Restore:

    kubectl delete ns jenkins vault
    velero restore create --from-schedule velero-jenkins-schedule    
    
On peut surveiller la restauration (InProgress ou Completed) avec :

    velero restore get
    
## Optionel : configurer la période de maintenance des repos restic sur S3

Lorsque un backup Velero arrive à expiration, il instruit Restic de marquer les snapshots correspondants comme étant `forget`.
Cependant le nettoyage effectif des snapshots n'a pas lieu immédiatement: il est nécessaire d'exécuter `restic prune` pour 
cela (voir ci-dessous). Velero se charge de ceci aussi, avec une périodicité de 168h par défaut.

Si on veut raccourcir ce délai il est possible d'éditer les ressources `resticrepositories.velero.io` manuellement. 
Il existe une instance de cette ressource par repository Restic. Dans notre cas il y en a un par namespace sous backup 
(cad un pour jenkins, un pour vault etc).

Il faut donc, par exemple pour jenkins, exécuter : 

    ```bash
    kubectl -n velero edit resticrepositories.velero.io  jenkins-default-lw9bq  (ou quelque soit son nom)
    ```   
et editer le champ `spec.maintenanceFrequency` à votre convenance.

Note personnelle: 
On peut normalement aussi utiliser `kubectl patch resticrepositories <repo> -n velero -p '{"spec":{"maintenanceFrequency":"24h0m0s"}} --type='json'`
mais chez moi ça marche pô :-(
    
    
---    
    
    
# Mode opératoire Restic

Normalement Velero se charge tout seul de piloter Restic. Toutefois en cas de soucis on peut être amené à intervenir 
manuellement sur Restic.

## Préparer le client Restic

Afin d'interragir avec le bucket il nous faut bien sûr un client Restic. Plutôt que d'en installer un from scratch on 
peut tout aussi bien réutiliser l'un des pods restic installé dans le namespace velero.

La procédure décrite ici vise à se connecter au pod, puis à configurer son environnement afin de pouvoir exécuter des 
commandes sur le bucket distant.   
 
1. Se connecter au pod :
    ```bash
    kubectl -n velero exec -it restic-<tab tab> -- /bin/bash
   ```

2. Paramétrer informations nécessaires

* Adresse du bucket: `s3:https://s3.fr-par.scw.cloud/newforge-velero/restic/jenkins` (ou `/vault`)
* api key ID : `cat /credentials/cloud | grep key_id | sed 's/.*= //g' | tr '\n' '\0' `
* secret key : `cat /credentials/cloud | grep secret | sed 's/.*= //g' | tr '\n' '\0' `
* clé chiffrage restic : `kubectl -n velero get secrets velero-restic-credentials -o jsonpath="{.data.repository-password}" | base64 --decode`

Note : cette dernière doit s'exécuter depuis l'extérieur du pod puisque restic n'a pas de client Kube à disposition.

On peut enfin tout renseigner dans le contexte du pod restic :

    ```bash
    unset HISTFILE
    export RESTIC_REPOSITORY="s3:https://s3.fr-par.scw.cloud/newforge-velero/restic/jenkins"
    export AWS_ACCESS_KEY_ID="<api key id>"
    export AWS_SECRET_ACCESS_KEY="<api secret>"
    export RESTIC_PASSWORD="<clé chiffrage restic>"    
    ```
    
Une fois que tout ceci est configuré on peut tester la connexion, par exemple avec `restic stats`

## lister les snapshots connus

    `restic snapshots`
    
## purger les snapshots ancien

Lorsqu'un backup Velero expire, les snapshots Restic correspondants sont marqués comme à oublier (la commande forget).
Il prévoit ensuite de les supprimer un peu plus tard (`prune`). On peut cependant le faire nous-même : 

    `restic prune`
    
source: https://restic.readthedocs.io/en/latest/060_forget.html