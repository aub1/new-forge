
# Aperçu général

## Description

Velero est un outil de backup permettant de sauver et restaurer des ressources du cluster, allant du simple volume à tout 
un namespace, et d'entreposer tout ça sur un serveur distant.

Une sauvegarde par Velero est constituée de deux composantes : 
* le backup des ressources (le yaml)
* les backups de volumes correspondants (eg des VolumeSnapshot). 

Les backups des ressources sont envoyées vers un bucket de type S3. Pour les images de volumes, ça dépend de comment sont 
faits nos volumes (voir plus bas).

On peut bien sûr configurer une politique de backup périodique et automatique, et les backups ont une durée de vie 
exprimée sous forme de TTL en heures (par défaut 720, soit environ 1 mois).

     
    Note : Velero permet le backup des ressources qui sous-tendent les **Workloads**. 
    Pour le backup **du control plane**, il faut aussi faire des backups de etcd. C'est une procédure séparée qui est documentée ici : 
    https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#backing-up-an-etcd-cluster


## Les setup Velero en fonction du type de volume

### TLDR 

Si vous ne comprenez pas cette table, veuillez lire la suite puis retenter plus tard.

Solution                    | Setup des Volumes
----------------------------+-----------------------------------------------------
VolumeSnapshots             | Kubernetes 1.17 + 
(natif Kubernetes)          | Volumes gérés par un provider CSI-compliant qui implémente VolumeSnapShot +
                            | plugin Velero pour support du CSI 
----------------------------|-----------------------------------------------------
Vendor-specific snapshots   | Volume servi par un provider particulier (Amazon etc) qui propose un service de snapshots +
(drivers "In Tree")         | plugin Velero pour support de ce provider
----------------------------|-----------------------------------------------------
Restic                      | Autres cas de figures :
                            |   - CSI provider qui n'implémente pas VolumeSnapshot, 
(avec un hic)               |   - Vendor-specific qui n'implémente pas de service snapshot
                            |   - Pas de plugin Velero approprié pour la techno cible
                            |
                            | Le hic: HostPath natif interdit (CSI+HostPath est permis)  


### Choses à connaître sur les volumes pour comprendre la suite

Les volumes dans Kubernetes sont gérés par des "providers". Les providers prennent en charge toutes les questions relatives 
 au montage physique, les I/O, éventuellement l'authentification si le volume est distant etc. Par exemple un volume chez 
 AWS n'est pas la même chose qu'un volume chez Google ou qu'un volume sur un disque local. 
 
Il en existe de deux types :
* "In Tree" : le code de ces providers est built-in dans le code de Kubernetes. C'est l'approche historique, mais aujourd'hui dépréciée.
* Container Storage Interface (CSI) : c'est une approche plugin, qui permet à chaque vendor de storage de publier lui-même son provider, 
et auquel Kubernetes peut complètement déléguer. C'est cette approche qui est favorisée aujourd'hui par la core team Kubernetes.


### Cas 1 et 2 : Les volumes peuvent être snapshotés par Velero

Un Snapshot est une image d'un volume. Mais avant de voir comment Velero fait un snapshots, voyons comment Kubernetes fait 
un snapshot. 

Dans Kubernetes un snapshot est une ressource de type VolumeSnapshot (en réalité : un VolumeSnapshotContent). Kubernetes 
supporte cette fonctionnalité à condition que le volume soit géré par CSI, et à condition que l'implémentation de CSI 
sous-jacente l'implémente réellement [1] .

Pour les providers In Tree, Kubernetes ne supporte pas les VolumeSnapshots. Heureusement, cela n'empêche pas 
le vendor tel que AWS ou GCP de permettre à ses clients de sauver ses disques en dehors de Kubernetes, 
c'est juste que Kubernetes ne s'en occupe pas.


Partant ce cela, Velero est en mesure de faire des VolumeSnapshots si [2] :

* Les volumes sont gérés par un provider CSI qui implémente VolumeSnapshot, auquel cas il peut s'appuyer sur Kubernetes directement, 
ou
* Les volumes sont gérés par un provider "In Tree", mais Velero peut contourner Kubernetes parce que :
    - Le provider propose indépendamment de Kubernetes un service de sauvegardes, et
    - Velero dispose d'un plugin (à installer en plus), capable d'utiliser ce service 


A noter que le montage nominal de Velero est visiblement le cas de contournement, probablement parce que la feature CSI de 
Kubernetes est relativement récente. D'ailleurs, le support de CSI par Velero est toujours en cours en développement (au moins 
pour Velero 1.5). 

#### Stockage distant des snapshots

Une fois que le/les snapshots sont effectués Velero, peut envoyer les backups vers leur stockage distant. On rappelle ici 
que les Snapshots de volumes ne sont qu'une composante d'un backup Velero : en effet le yaml des ressources Kubernetes 
(deployment etc) sont sauvegardées dans une archive distincte. Pour cette raison, il est possible (ou obligatoire ?) de 
sauvegarder les deux composantes dans des entrepôts différents [4] :
 
* les ressources vont sur un bucket S3 ("Backup Location"),
* les snapshots sont hébergés ailleurs ("Snapshot Location")


Sources :
* [1] https://kubernetes.io/docs/concepts/storage/volume-snapshots/
* [2] https://velero.io/docs/v1.5/supported-providers/
* [3] https://velero.io/docs/v1.5/csi/
* [4] https://velero.io/docs/v1.5/locations/


### Cas 3 : les volumes ne peuvent pas être Snapshoté par Velero

Pour tout autre cas de figure Velero propose une integration avec Restic [1]. Restic est un outil de backup de base niveau : 
il agit directement à partir du filesystem. Pour cette raison un backup par Restic ne fait pas d'image d'un volume à 
proprement parler : il s'agit plutôt d'une grosse tarball.

Note : il y a une exception, si le volume est un Volume ou PersistentVolume a un driver In Tree en mode HostPath.
Par contre si le volume est un PersistentVolume servi par un provider CSI, ça marche même s'il est en mode HostPath. 

#### Stockage distant des snapshots
Dans cette situation, le serveur distant qui accueille les tarball n'est pas distinct de celui qui accueille les backups 
de ressource (contrairement au cas 1 et 2) : les deux vont dans un même bucket S3 [2].

Sources :
* [1] https://velero.io/docs/v1.5/supported-providers/#non-supported-volume-snapshots
* [2] https://velero.io/docs/v1.5/locations/#limitations--caveats

## Note à propos des bucket S3

Velero envoie ses backups sur des buckets (et parfois les snapshots avec en fonction des cas, voir ci-dessus), cependant 
le endpoint réel derrière a une importance quand même car Velero a besoin d'un plugin pour interagir avec. En conséquence 
tous les bucket S3 ne sont pas supportés [1]. 

Il se trouve que malgré tout le S3 que nous utilisons, chez Scaleway, est bien compatible avec Velero 
(en utilisant le plugin aws).

Sources :
* [1] https://velero.io/docs/v1.5/supported-providers/#s3-compatible-object-store-providers



----------------------------

# Tests avec Restic

## Prerequis

Afin de tester le plus simple est d'avoir un minikube, avec un Jenkins dedans + autres ressources dans le même 
namespace. Afin de ne pas risquer d'exécuter sur la prod nous choisirons le namespace `poc` plutôt que `jenkins`.

A noter que pour ce test Jenkins nécessite que jenkins-home soit monté sur un volume adapté, ici un volume Local. Il est 
donc conseillé d'avoir au préalable configuré les volumes comme indiqué dans `cluster-global/storage/config-volumes.md`.


## Installation

Une fois l'environnement de test monté on peut installer la démo. Ceci est le mélange de la page d'installation de 
la démo [1] + celle de l'utilisation de Restic [2] que l'on trouve sur le site de Velero. Nous utilisons donc ici 
comme stockage des backups un Minio configuré minimalement + services etc tels que fourni dans le package d'exemples 
fourni avec la distribution de Velero.

Pour se simplifier la vie, le reste de cette section suppose que vous faites tout en sudo.

Download de Velero et installation du binaire :

    wget https://github.com/vmware-tanzu/velero/releases/download/v1.5.1/velero-v1.5.1-linux-amd64.tar.gz
    tar -xvf velero-1.5.1-linux-amd64.tar.gz -C /opt
    
Installation des ressources Velero + setup du serveur Minio dans le namespace `velero` :

    cd /opt/velero-v1.5.1-linux-amd64
    mv velero /usr/bin
    
    cat << 'EOF' > credentials-velero
    [default]
    aws_access_key_id = minio
    aws_secret_access_key = minio123    
    EOF
    
    kubectl apply -f examples/minio/00-minio-deployment.yaml

    velero install \
      --provider aws \
      --plugins velero/velero-plugin-for-aws:v1.0.0 \
      --bucket velero \
      --secret-file ./credentials-velero \
      --use-volume-snapshots=false \
      --default-volumes-to-restic \
      --use-restic \
      --backup-location-config region=minio,s3ForcePathStyle="true",s3Url=http://minio.velero.svc:9000   

Note : nous utilisons ici le mode `--default-volumes-to-restic`, qui a son importance comme on le verra plus bas.

On peut surveiller le déploiement avec :

    kubectl get deployments -l component=velero -n velero
    kubectl get pods -n velero -w 

Puisque nous utilisons Restic nous devons annoter chaque pod qui monte un volume à snapshoter pour 
indiquer quels volumes nous intéressent. Dans le cas de notre jenkins, le seul vrai volume à exploiter
s'appelle `jenkins-home` :

    kubectl -n poc annotate pod/jenkins-blabla-blabla backup.velero.io/backup-volumes-excludes=tmp,plugins,plugin-dir

Nous avons maintenant un setup Velero+Restic+Minio complet, et le volume du home-directory de jenkins sera 
embarqué dans les backups.

Note : dans le cas de Minikube ou Rancher il n'y a pas besoin de travail supplémentaire pour configurer 
l'intégration Restic. Cependant dans d'autres distro il y aurait un peut plus de travail.

Note 2 : ici l'annotation velero qu'on a posé sur le pod de Jenkins est la solution opt-out, où par défaut tous les volumes sont 
embarqués, hormis ceux qu'on spécifie explicitement comme ici (les configmap et secrets sont exclus par défaut). 
Il existe un mode opt-in où on fait l'inverse : on doit déclarer explicitement les volumes qui doivent êtres sauvegardés. 
Le mode opt-out est ici activé par `--default-volumes-to-restic`.

## Exemple d'utilisation

Pour faire un backup de tout le namespace :

    velero backup create mon-backup --include-namespaces poc
    
Vérifier que le contenu est bien arrivé sur MinIo :

    kubectl -n velero exec -it minio-blabla-blabla -- /bin/sh
    find /storage
    exit

Maintenant on sort la massue : 

    kubectl delete ns poc
    
On attend un peu puis on vérifie au retour du prompt que tout a bien disparu :

    kubectl get namespaces | grep poc
    
On peut maintenant restaurer tout le namespace :

    velero restore create --from-backup mon-backup

Vérifier que le namespace est bien restauré : 

    velero restore get
    kubectl -n poc get pods -w
    

## Pros et Cons 

Pour : 
* Solution complètement agnostique : pas besoin de configurer des StorageClass, d'activer des --feature-gates, 
 migrer nos volumes etc. 
* Puisque c'est agnostique, on peut donc exporter un namespace depuis la prod et remonter dans un espace preprod, ou encore 
depuis Rancher vers Minikube etc.
* Nous permet de continuer à utiliser des volumes stockés sur disque, donc permet de bidouiller à la main des 
choses sur le worker node en dernier recours si vraiment il faut.
    
Contre :
* Pour le stockage des backups on est limité à certains providers S3. Est-ce que ça marchera avec 
le S3 de Scaleway par exemple ? 
* On reste avec la StorageClass par défaut de Rancher (non CSI), or l'avenir des volumes Kubernetes est 
clairement tourné vers les volumes CSI


Note : pour le côté "Remonter partout", on risque quand même d'avoir un problème avec notre volume car dans sa définition 
il y a une affinité avec le node 'worker-1'. Il se pourrait donc qu'en restaurant le volume sur un autre cluster 
on rencontre un problème si aucun node 'worker-1' n'existe. 

Sources :
* [1] https://velero.io/docs/v1.5/contributions/minio/
* [2] https://velero.io/docs/v1.5/restic/


----------------
# Vieilles notes

# Montage technique

## Support des snapshots

Le cluster doit tout d'abord permettre de prendre des snapshots. C'est une feature que Kubernetes propose depuis pas longtemps. 
et est passée en beta. Ceci signifie qu'elle est assez mûre pour être utilisée en prod. Elle est sensée être active par défaut,
mais ça ne semble pas être le cas pour la distribution Rancher; il faut donc l'activer avec le flag 
`--feature-gates="VolumeSnapshotDataSource=true"` (quitte à redémarrer derrière).

Ce n'est pas tout : une fois la feature activer il reste encore à définir un VolumeSnapshotClass, car il n'en existe pas 
par défaut. 

    Note : je ne suis pas encore sûr de comment on fait ça, je suppose qu'on peut pour commencer calquer la définition 
    sur celle de la storage class par défaut. Sinon dans la doc la proposition est de passer par un provider CSI. Je sais 
    pas, faut creuser. 

## Volumes

La faculté de prendre des snapshots dépend de comment sont faits les volumes dans le cluster. 

Velero ne permet de prendre de snapshots que pour certaines types de volumes, typiquement des disk fourni par des gros 
hébergeurs Cloud (voir https://velero.io/docs/v1.5/supported-providers/), ou providers s'ils implémentent l'interface CSI.

Pas de chance, dans notre cas, nos volumes (qui sont fondamentalement des répertoires sur le disque de la VM) sont 
 définis comme suit :
* type : PersistentVolume
* provider : "in tree", cad pas du CSI
* storage class : par défaut (s'appelle "local-path" sur Rancher, "standard" sur Minikube. En gros c'est HostPath, 
    avec une affinité sur le worker pour forcer le pod consumer à démarrer sur le même worker. )

Et donc a priori ça ne marcherait pas dans ce contexte. On peut alors : 
* Soit intégrer Restic, un outil bas-level pour faire du backup de répertoire, 
* Soit changer de driver de stockage pour prendre un truc compatible (eg provider CSI)

# A zieuter : 

Tutos :
* https://blog.kubernauts.io/backup-and-restore-of-kubernetes-applications-using-heptios-velero-with-restic-and-rook-ceph-as-2e8df15b1487

Autres outils : 
* longhorn (d'origine rancher, très récent) : https://longhorn.io/docs/1.0.2/
* Rook : https://rook.io/docs/rook/v1.4/ceph-csi-snapshot.html

# Prérequis

En général Stash et Velero travaillent sur des Volumes qui sont par nature *cloud native*

Comme nous utilisons des PersistentVolume (qui plus-est, sous-tendus par un dossier local au node), Stash et Velero laissent le choix 
entre deux propositions :
* Soit utiliser Restic, 
* Soit utiliser la feature Kubernetes `VolumeSnapshotDataSource` conjointement avec le plugin CSI (Container Storage Interface). Il faut 
donc les activer si nécéssaire (avec le flag `--feature-gates="VolumeSnapshotDataSource=true").

