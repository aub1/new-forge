# Monitoring du cluster Kube

## Objectif :

Le but est d'avoir accès à un/des dashboards pour le monitoring du cluster de prod. Le but premier est de donner des indicateurs sur l'état de santé de notre cluster :

- Santé "Bare Metal" : CPU, RAM, Disk, Network, ...
- Santé "Kubernetes" : Deploiements, Pods, Replicas, ... 

La solution se base sur Prometheus (CNCF) + Grafana :

- Prometheus pour la récupération de métriques
- Grafana pour les jolis dashboards

   

## Pré-requis :

1. Helm doit être installé sur le client qui de la personne qui va effectué le déploiement. En effet ce dernier va se faire via Helm. 
2. Le contexte de kubectl doit pointer sur celui de la nouvelle forge. Si ce n'est pas le cas utiliser :
   > kubectl config use-context <newforge> 
3. Le dépôt Helm par défaut https://charts.helm.sh/stable doit être présent
   > helm repo add stable https://charts.helm.sh/stable
4. Le namespace monitoring doit être présent. Si ce n'est pas le cas 
   > kubectl create namespace monitoring
   
## Procédure:

1. Déployer Prometheus
   
    a. Récupérer le fichier "prometheus-values.yaml". Celui-ci a été modifié pour rendre possible le backup de la data par Velero/Restic. Il s'agit pour cela d'utiliser la "storage class" "openebs-hostpath" tel qu'indiqué dans cette section. En fait il y a trois sections car trois "persistent volume" en tout : Pushgateway, Alertmanager, Server): 
	  
	  ``` yaml
	   persistentVolume:
	       ## If true, alertmanager will create/use a Persistent Volume Claim
	       ## If false, use emptyDir
	       ##
	       enabled: true
	   
	       ## alertmanager data Persistent Volume access modes
	       ## Must match those of existing PV or dynamic provisioner
	       ## Ref: http://kubernetes.io/docs/user-guide/persistent-volumes/
	       ##
	       accessModes:
	         - ReadWriteOnce
	   
	       ## alertmanager data Persistent Volume Claim annotations
	       ##
	       annotations: {}
	   
	       ## alertmanager data Persistent Volume existing claim name
	       ## Requires alertmanager.persistentVolume.enabled: true
	       ## If defined, PVC must be created manually before volume will be bound
	       # existingClaim: ""
	   
	       ## alertmanager data Persistent Volume mount root path
	       ##
	       mountPath: /data
	   
	       ## alertmanager data Persistent Volume size
	       ##
	       size: 2Gi
	   
	       ## alertmanager data Persistent Volume Storage Class
	       ## If defined, storageClassName: <storageClass>
	       ## If set to "-", storageClassName: "", which disables dynamic provisioning
	       ## If undefined (the default) or set to null, no storageClassName spec is
	       ##   set, choosing the default provisioner.  (gp2 on AWS, standard on
	       ##   GKE, AWS & OpenStack)
	       ##
	       storageClass: "openebs-hostpath"
	  ```
	  
    b. Déployer via helm prometheus 
        > helm upgrade --install prometheus -f prometheus-values.yaml -n monitoring stable/prometheus

2. Déployer Grafana 

    a. Récupérer le fichier "grafana-values.yaml". Celui-ci a été modifié pour rendre possible le backup de la data par Velero/Restic. Il s'agit pour cela d'utiliser la "storage class" "openebs-hostpath" tel qu'indiqué dans cette section: 
	  
	  ``` yaml
	  ## Enable persistence using Persistent Volume Claims
	  ## ref: http://kubernetes.io/docs/user-guide/persistent-volumes/
	  ##
	  persistence:
	    type: pvc
	    enabled: true
	    storageClassName: "openebs-hostpath"
	    accessModes:
	      - ReadWriteOnce
	    size: 10Gi
	    # annotations: {}
	    finalizers:
	      - kubernetes.io/pvc-protection
	    # subPath: ""
	    # existingClaim:
	  ```

    b. Il a été aussi modifié pour que grafana puisse connaitre son URL

      ``` yaml
	   grafana.ini:
          server:
            # The full public facing url you use in browser, used for redirects and emails
            root_url: https://demo.new-forge.squashtest.org/grafana
      ```

	c. Déployer via helm Grafana
	   > helm upgrade --install grafana -f grafana-values.yaml -n monitoring stable/grafana

3. Accès à Grafana

    a. Récupération du secret admin 
       > kubectl get secret --namespace monitoring grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
	  
    b. A l'étape prévédente le values du chart Helm de Grafana a été modifié pour que ce dernier connaisse son url. 
    
      ``` yaml
       grafana.ini:
          server:
            # The full public facing url you use in browser, used for redirects and emails
            root_url: https://demo.new-forge.squashtest.org/grafana
      ```

    c. Déployer l'ingress graphana-ingress.yaml dans le namespace monitoring
       
       > kubectl apply -f graphana-ingress.yaml -n monitoring
    
    d. Accéder à l'instance via soon url https://demo.new-forge.squashtest.org/grafana


4. Visulatisation des dashboards

    a. Ajouter la source : http://prometheus-server:80
    b. Dans Dashboard/manage ajouter les tableaux 1860 et 8685.