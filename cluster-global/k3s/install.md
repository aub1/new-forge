# Nouvelle forge

## K3S

La nouvelle forge est construite, au moins pour sa partie Jenkins et SonarQube, sur un cluster Kubernetes.

L'équipe administration système a créé deux domaines :

- jenkins.new-forge.squashtest.org:443
- new-forge.squashtest.org:6443

qui réfèrent le cluster.  Le premier domaine, ouvert au monde, permettra d'accéder au service Jenkins, le second, ouvert seulement au domaine 'henix', permettra les opérations d'administration du cluster.

## Installation du serveur (en tant que service)

### Préambule

Nous sommes sur des VM debian 10 modernes.  k3s, à ce jour, ne gère pas la nouvelle manière de gérer les iptables de cette release.

Il faut donc basculer vers la version 'legacy' de iptables sur les noeuds du cluster :

    sudo iptables -F
    sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
    sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
    sudo reboot

Et donc rebooter les noeuds.

### Installation de k3s

Il semblerait qu'il ne soit pas possible, dans la version actuelle, de spécifier le bind-address directement : cela empêche la génération du
kubeconfig :-(

Il faut donc procéder en deux étapes :

    curl -sfL https://get.k3s.io | sh -s - --docker

Puis modifier le fichier ` /etc/systemd/system/k3s.service` et ajouter l'option `--bind-address` comme suit :

```systemd
[Unit]
Description=Lightweight Kubernetes
Documentation=https://k3s.io
Wants=network-online.target

[Install]
WantedBy=multi-user.target

[Service]
Type=notify
EnvironmentFile=/etc/systemd/system/k3s.service.env
KillMode=process
Delegate=yes
# Having non-zero Limit*s causes performance problems due to accounting overhead
# in the kernel. We recommend using cgroups to do container-local accounting.
LimitNOFILE=1048576
LimitNPROC=infinity
LimitCORE=infinity
TasksMax=infinity
TimeoutStartSec=0
Restart=always
RestartSec=5s
ExecStartPre=-/sbin/modprobe br_netfilter
ExecStartPre=-/sbin/modprobe overlay
ExecStart=/usr/local/bin/k3s \
    server \
        '--bind-address' \
        '0.0.0.0' \
```

Puis relancer le service :

    systemctl daemon-reload
    systemctl restart k3s

## Installation de kubectl

Installer si besoin kubectl sur le noeud serveur :

    curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl

    chmod +x ./kubectl
    mv ./kubectl /usr/local/bin/kubectl

Redéfinir ensuite la variable d'environnement `KUBECONFIG` :

    export KUBECONFIG=/etc/rancher/k3s/k3s.yaml

Si on souhaite utiliser kubectl à partir de son poste, il suffira de recopier ou importer ce fichier de paramètre dans le fichier `.kube/config` local.  (Ne pas oublier de changer l'adresse du serveur, qui devra être le nom de machine indiqué en introduction).

## Finalisation du noeud master

Ajouter une teinte pour limiter le scheduling sur ce noeud sensible : 

    kubectl taint nodes forge-kube-master node-role.kubernetes.io/master=:NoSchedule

## Installation des workers

### Préambule

Nous sommes sur des VM debian 10 modernes.  k3s, à ce jour, ne gère pas la nouvelle manière de gérer les iptables de cette release.

Il faut donc basculer vers la version 'legacy' de iptables sur les noeuds du cluster :

    sudo iptables -F
    sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
    sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
    sudo reboot

Et donc rebooter les noeuds.

### Installation de k3s

Définir les variables d'environnement `K3S_URL` et `K3S_TOKEN` comme suit :

    export K3S_TOKEN=<token>
    export K3S_URL=https://10.0.0.200:6443

Le token à utiliser est sur le serveur dans le fichier suivant :

    /var/lib/rancher/k3s/server/node-token

Une fois ces variables d'environnement spécifiées, installer k3s :

    curl -sfL https://get.k3s.io | sh -s - 

NOTE : dans le cas de forge-worker-3 qui est dédiée à Squash-TM il faut donner une teinte (comme pour le master). 
Du coup la commande devient : 

    curl -sfL https://get.k3s.io | sh -s - --node-taint node-role.henix.com/dedicated-node=squashtm:NoSchedule



Donner un role au noeud (optionel, mais va simplifier la suite) :

    kubectl label node <node> node-role.kubernetes.io/worker=worker
    


### Vérification

Il faut maintenant vérifier si le noeud est bien enregistré auprès du serveur.  Exécuter la commande suivante sur le master (ou un poste configuré avec kubectl et le fichier de configuration associé) :

    kubectl get nodes

Le résultat doit être quelque chose comme :

    $ kubectl get nodes
    NAME                STATUS   ROLES    AGE   VERSION
    forge-worker-1      Ready    <none>   17m   v1.18.6+k3s1
    forge-kube-master   Ready    master   24m   v1.18.6+k3s1


# Teintes

Le cas échéant, pensez à rajouter les `tolerations` aux daemons légitimes. Se référer au fichier `taints.md` pour les 
détails.
