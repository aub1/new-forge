# Teintes

Nous définissons les teintes suivantes sur le cluster :

Teinte                                        | Signification
---------------------------------------------:|:--------------------
node-role.kubernetes.io/master:NoSchedule     | Ne scheduler que le control-plane et les daemonset
node-role.henix.com/dedicated-node=<REALM>:NoSchedule | Le noeud est réservé aux pods liés au REALM et les daemonsets

# Noeuds teintés

Certains noeuds ont des teintes (`taint` ) afin de restreindre l'accès au scheduling. La liste des noeuds teintés est :

Node                | Teinte                                        
-------------------:|:---------------------------------------------
forge-kube-master   | node-role.kubernetes.io/master:NoSchedule     
forge-worker-3      | node-role.henix.com/dedicated-node=squashtm:NoSchedule (uniquement le build de TM) 


# Daemons

La plupart des daemons sont légitimes à exécuter des pods sur tous les noeuds quelques soient leur teintes. Cela signifie 
qu'ils devraient recevoir les `tolerations` correspondantes.

Actuellement La liste de nos daemons approuvés est :

namespace       | pod-name
---------------:|:------------------------
kube-system     | svclb-traefik             !! Impossible de donner des tolérations : https://github.com/k3s-io/k3s/issues/1988
monitoring      | prometheus-node-exporter
velero          | restic
openebs         | openebs-ndm
logging         | loki-promtail
 
Afin de leur donner les permissions nécessaires, penser à éditer le cas échéant les helm charts correspondants
(à noter que la Toleration pour la teinte `node-role.kubernetes.io/master` semble déjà inclus de base car c'est un standard).

Autrement, éditer le daemonset afin de rajouter la toleration, par exemple :  

    kubectl -n <namespace> patch daemonset <daemonset> \
        --patch '{"spec":{"template":{"spec":{"tolerations":[{"key":"node-role.henix.com/dedicated-node", "operator": "Exists"}]}}}}'
    
C'est pas testé, mais au pire éditez via `kubectl edit` :-) voici l'équivalent yaml à insérer :

    spec:
      template:
        spec:
          tolerations:
          - key: "node-role.henix.com/dedicated-node"
            operator: "Exists"

   
Tips : Afin de ne pas s'embêter, utiliser l'opérateur Exists. La Toleration s'appliquera à l'ensemble des valeurs possible de la teinte.