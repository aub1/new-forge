# GOTCHA 

La procédure de création telle que décrite ici par exemple ([1]) est globalement correcte. Cependant il y a un lol :
le ticket [2] nous dit que la commande `kubectl certificate approve <csr>` va signer la csr avec le mauvais certificat 
(au moins dans k3s). 

Il faut donc signer la csr à la main depuis le noeud maître. Pour cela : 
* rappatrier la csr sur le noeud maître, 
* signer avec 
    ```shell script
    openssl x509 -req -in cert-req.csr \
      -CA /var/lib/rancher/k3s/server/tls/client-ca.crt \
      -CAkey /var/lib/rancher/k3s/server/tls/client-ca.key \
      -CAcreateserial \
      -out certif.crt -days 3650
    ```
* Puis rappatrier le certificat signé pour que le user puisse l'inclure dans son kube config.



Sources :
* [1] https://medium.com/better-programming/k8s-tips-give-access-to-your-clusterwith-a-client-certificate-dfb3b71a76fe
* [2] https://github.com/rancher/k3s/issues/684