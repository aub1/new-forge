# Role et Clusterrole

Ce répertoire ne contient que les Clusterroles, cad de scope cluster. Pour les rôles restreint à un namespace donné 
veuillez vous référer au répertoire contenant les ressources de ce namespace.

Kubernetes fait la distinction entre les `roles` (qui a une portée limitée à son namespace) et les `clusterroles` 
(dont le scope est le cluster entier). Quand on créée un namespace on peut éventuellement y rajouter les roles indiqués 
ici. 

Les fichiers `-clusterrole.yaml` décrivent les clusterroles. Les `clusterroles` sont a priori à n'instancier qu'une fois, 
au moment où le cluster est créé. Les fichiers `*-role.yaml` représentent les `roles`, si nécessaire il faut alors les créer 
une fois par namespace.

Normalement on n'a pas besoin de créer de role quand les roles/clusterroles de base par défaut sont suffisant. Les 
clusterroles de base peuvent être listés via `kubectl get clusterroles` et inspectés via `kubectl describe clusterrole <nom>`.


# Role bindings 

Les fichiers `*-rolebinding.yaml` et `*-clusterrolebinding.yaml` donnent à des utilisateurs/groupes le/les rôles donnés. 
Ils sont listés au format `<namespace>-<role>-rolebinding.yaml`.




Sources :
* https://kubernetes.io/docs/reference/access-authn-authz/rbac/
* https://kubernetes.io/docs/reference/access-authn-authz/rbac/#default-roles-and-role-bindings

