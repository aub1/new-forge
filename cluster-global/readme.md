# Top Level configuration

Les éléments de configuration top-level devraient être appliqués avant installation des helm charts / deployments etc.

## Manifeste

### Roles 

Voir documentation : https://kubernetes.io/docs/reference/access-authn-authz/rbac/

* secret-reader-clusterrole : c'est un ClusterRole (qui existe globalement donc) qui permet aux sujets qui en bénéficient de lire les secrets. Note : on peut restreindre la portée d'un tel role à un namespace donné via un RoleBinding (plutôt qu'un ClusterRoleBinding).


## Installation

    kubectl apply -f secret-reader-clusterrole.yaml