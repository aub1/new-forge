# Concentration des logs du cluster 

## Objectif :

Le but est d'avoir accès l'ensemble des logs produits par le cluster et les déploiements

La solution se base sur Promtail + Loki + Grafana :

- Promtail : Comme prométhéus pour les métriques systèmes, promtail permet de faire du scrapping sur les logs. 
- Loki :  Va centraliser/traiter/indexer les logs
- Grafana : Va permettre de requêter/afficher ces logs

## Pré-requis :

1. Helm doit être installé sur le client qui de la personne qui va effectué le déploiement. En effet ce dernier va se faire via Helm. 
2. Le contexte de kubectl doit pointer sur celui de la nouvelle forge. Si ce n'est pas le cas utiliser :
   > kubectl config use-context <newforge> 
3. Le dépôt Helm de loki https://grafana.github.io/loki/charts doit être présent
   > helm repo add loki https://grafana.github.io/loki/charts
   > helm repo update
4. Le namespace logs doit être présent. Si ce n'est pas le cas 
   > kubectl create namespace logs

## Procédure:

### Optionnel 

Si vous voulez utiliser les dernières sources pour le helm loki alors cette section est pour vous sinon, utilisez les ressources commités avec ce projet et tout ira bien. 

1. Récuperer les values
   > helm inspect values loki/loki-stack > loki-values.yaml
   > 

2. Dupliquez le fichier de values fourni pour pouvoir le customisé
   > cp values.yaml loky-values.yaml

3. Rajouter l'annotation pour exclure le storage de backup valero (on verra si par la suite on joue sur la persistence/backup)
   
   ``` yaml

   loki:
     podAnnotations:
       backup.velero.io/backup-volumes-excludes: "storage"

   ```
4. Pour l'instant il n'y a pas de persistence (on est sur du empty dir). La section persistence a qaund même été rajouté au chart helm pour le futur au cas où
   ``` yaml

   loki:
     persistence:
       enabled: false
       accessModes:
        - ReadWriteOnce
       size: 10Gi
       annotations: {}
       # selector:
       #   matchLabels:
       #     app.kubernetes.io/name: loki
      # subPath: ""
      # existingClaim:

   ```

### Install

1. Les values sont auto porteuses. On peut donc directement rouler : 
   > helm upgrade --install loki loki/loki-stack -n logging -f loki-values.yaml

2. Et voilà c'est fini. Vérifié que les pods et services ont été crées dans le namespace logging

### Configuraiton dans grafana

1. Accéder à la GUI Grafana via son URL
2. Dans la section "datasources" rajouter une datastource de type "Loki"
3. Rentrez l'url http://loki.logging.svc.cluster.local:3100
4. C'est bon c'est configuré

### Usage

L'utilisation la plus simple se fait par la section "explore" en selectionnant la datasource "Loki". Il y a une cheatsheet des requetes que l'on peut effectuer. 

Après comme tout dans graphana on peut consolider ces requetes dans des dashboards.

