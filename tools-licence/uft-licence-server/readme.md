# Serveur de licence UFT

Dans le cadre des évolutions de Squash Autom/Squash DevOps, un serveur de licence UFT a été ajouté dans la forge.

Celui-ci est présent dans son propre namespace `tools-licence`.

### Déploiement et utilisation

Le fichier yaml présent dans ce même dossier suffit à lui-même pour le déploiement, aucune opération supplémentaire n'est nécessaire.

L'application est accessible (uniquement via VPN) à l'URL suivante : `autopass.new-forge.squashtest.org:5814/autopass` 

### Composition

Ce fichier permet de créer les éléments suivants :

Deux PVC de type openebs-hostpath : 

  * pvc-uft-licence-server-config pour contenir la configuration du serveur
  
  * pvc-uft-licence-server-data pour contenir les données du serveur

Déploiement d'un container seul mfsharedtech/apls en privileged avec les deux volumes montés.

Un service sur le port 5814 avec IP externe sur le worker.

### Problèmes connus

L'IP externe a été utilisée suite à certaines problématiques rencontrées lors de l'utilisation via ingress.

Le container n'accepte actuellement que les connexions HTTPS. L'apache faisant la redirection en HTTP, ceci a causé les premiers soucis.

L'ingress a également eu des soucis de redirection vers le service ce qui provoquait des Internal Server Error.

Pour contourner ce soucis, un VHost pointant sur l'IP externe a été ajouté sur l'apache de l'hyperviseur avec une nouvelle URL.

