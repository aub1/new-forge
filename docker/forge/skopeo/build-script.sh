#!/bin/sh

#First step : register skepoe's package source
apt update
apt install -y curl gnupg
echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_10/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_10/Release.key | apt-key add - 
apt update

# Second step : installing skopeo
apt install -y skopeo

# Third step : installing git in case we need to commit/push a file to record used image digests
apt install -y --no-install-recommends git
git config --global user.email jenkins@squashtest.org
git config --global user.name "Forge"

# Last step : removing anything that is not useful for skopeo operations
apt remove --purge curl gnupg
apt autoremove --purge
apt clean
rm /build-script.sh