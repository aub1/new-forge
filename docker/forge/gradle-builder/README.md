Cette image contient Gradle couplé à une jdk 8.

Git est déjà inclus.

Un fichier init.gradle est injecté dans /home/gradle/.gradle qui permet de faire la connexion au dépôt Nexus.

L'authentification au dépôt Nexus se fait via les variables d'environnements NEXUS_USERNAME et NEXUS_PASSWORD.

Cette image permet uniquement de pull depuis Nexus, le push n'est pas configuré.

L'utilisateur par défaut de l'image est "gradle".

Pour information, le fonctionnement de Gradle est différent de celui de Maven.

Le dépôt déclaré dans init.gradle prend la priorité sur ceux du build.gradle des projets mais si le Nexus n'est plus accessible (ou que les variables d'environnements ne sont pas injectés),

alors Gradle basculera sur les autres dépôts à disposition.
