#!/usr/bin/env bash

# set umask for sftp
UMASK=${UMASK:-022}
sudo sed -i "s|/usr/lib/ssh/sftp-server$|/usr/lib/ssh/sftp-server -u ${UMASK}|g" /etc/ssh/sshd_config

# set key auth in file
if [ ! -f /home/squashtf/.ssh/authorized_keys ];then
    touch /home/squashtf/.ssh/authorized_keys
fi

# change port from default if requested
if [ "$SSHD_PORT" != "" ]; then
    echo "Port $SSHD_PORT" | sudo tee /etc/ssh/sshd_config.d/00sshdPort.conf
fi

# switch to password connexion if requested
if [ "$PASSWORD_ACCESS" = "true" ] \
    && [ "$USER_NAME" != "" ] && \
    [ "$USER_PASSWORD" != "" ]; then
    sudo sed -i "s/PasswordAuthentication no/PasswordAuthentication yes/g" /etc/ssh/sshd_config
    sudo useradd -m $USER_NAME
    sudo usermod -p $(echo "$USER_PASSWORD" | openssl passwd -stdin) $USER_NAME
    echo "export JAVA_HOME=${JAVA_HOME}" | sudo tee -a /etc/environment
    echo "export PATH=${PATH}" | sudo tee -a /etc/environment
fi

[[ -n "$PUBLIC_KEY" ]] && \
    [[ ! $(grep "$PUBLIC_KEY" /home/squashtf/.ssh/authorized_keys) ]] && \
    echo "$PUBLIC_KEY" >> /home/squashtf/.ssh/authorized_keys && \
    echo "Public key from env variable added"

[[ -n "$PUBLIC_KEY_FILE" ]] && [[ -f "$PUBLIC_KEY_FILE" ]] && \
    PUBLIC_KEY2=$(cat "$PUBLIC_KEY_FILE") && \
    [[ ! $(grep "$PUBLIC_KEY2" /home/squashtf/.ssh/authorized_keys) ]] && \
    echo "$PUBLIC_KEY2" >> /home/squashtf/.ssh/authorized_keys && \
    echo "Public key from file added"

# permissions
chmod go-w \
    /home/squashtf
chmod 700 \
    /home/squashtf/.ssh
chmod 600 \
    /home/squashtf/.ssh/authorized_keys